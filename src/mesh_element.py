#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 17:04:33 2021

@author: garciahahn
"""
import numpy as np
from multivariable_element import *

HERMITIAN_IDX = np.array([[0, 1], [-2, -1]])

class CartesianMeshElement():
    def __init__(self, ID, coords):
        self.coords = coords # coords have to be a dim x 2 dimensional array with
                             # the extremes of the cartesian element on each dimension
        self.ID = ID
        self._dim = len(coords)
        self.neighbour = np.empty(3 ** self._dim - 1, dtype = object)
        self.neighbour_idx = 0
        self.boundaries = np.zeros([self._dim, 2], dtype = bool)
        self._jacobian = None
        self._space = None
        self._cont = None
        self.coords = np.array(self.coords)
        self._jacobian = np.array([np.squeeze(np.abs(np.diff(self.coords[i, :])))/2 for i in range(self._dim)])
        self._alpha = None
        self.K = None
        self.G = None
        self.K_bc = None
        self.G_bc = None
        

    def _headless_attach(self, direction):
        pass
    
    def __str__(self):
        str1 = "I'm element {:d}".format(self.ID)
        idx = np.nonzero(self.boundaries)
        bound = self.boundaries[idx]
        for i in zip(*idx):
            str1 += "\nI am in a boundary at direction {:d} in the {:s} limit".format(i[0], "bottom" if i[1] == 0 else "top")
        return str1
        
    def attach(self, direction, element): # I think it is deprecated
        if not (self.neighbour_idx >= self.neighbour.size):
            direction = np.array(direction)
            self.neighbour[self.neighbour_idx] = np.array([direction, element.ID], dtype = object)
            self.neighbour_idx += 1
            
            element.neighbour[element.neighbour_idx] = np.array([-direction, self.ID], dtype = object)
            element.neighbour_idx += 1
        else:
            print("Cannot admit more elements as neighbours!")
            
    def isInside(self, point):
        point = np.array(point)
        if len(point) != self._dim:
            raise ValueError("point should be of the same dimensions as"
                             " the problem. Problem is of dimension {:d} and "
                             "point has dimension {:d}.".format(
                                                         self._dim,
                                                         len(point),
                                                         ))
        ret = (self.coords.T - point).T
        ret = np.sign(ret)
        ret = np.squeeze(np.diff(ret))
        ret = np.any(ret == 0)

        return ~ret


    def getContinuityDofs(self, direction, sense, derivative):
        if self._space._basis_function == BasisFunction.Lagrange:
            if derivative == 0:
                mask = np.zeros(self._space._dof[direction])
                mask[0 if sense < 0 else -1] = 1
            else:
                raise ValueError("Lagrangian basis have not independent "
                                 "degrees of freedom at their boundaries for"
                                 " first derivative values.\n"
                                 "Ist es: they can only accept value "
                                 "continuity (derivative must be 0)")
        elif self._space._basis_function == BasisFunction.Hermite:
            if 0 <= derivative < 2:
                mask = np.zeros(self._space._dof[direction])
                mask[HERMITIAN_IDX[
                        0 if sense < 0 else 1,
                        derivative,
                        ]] = 1
            else:
                raise ValueError("Hermitians basis have not independent "
                                 "degrees of freedom  at their boundaries "
                                 "for second derivative values.\n"
                                 "dervative argument must be 0 or 1.")
        ret = 1
        for i in range(self._dim):
            if i == direction:
                ret = np.kron(ret, mask)
            else:
                ret = np.kron(ret, np.ones(self._space._dof[i]))
        return np.nonzero(np.tile(ret, self._space.var_n))

    def getWeightMatrix(self):
        return self._space.getWeightMatrix(self._jacobian)

    def getWeightMatrixForDir(self, direction):
        return self._space.getWeightMatrixForDir(direction, self._jacobian)
        
    def getFieldOperator(self, derivative):
        return self._space.getFieldOperator(derivative, self._jacobian)

    def getBoundaryOperator(self, direction, boundary, derivative):
        return self._space.getBoundaryOperator(direction, boundary, derivative, self._jacobian)
    
    def getPointOperator(self, derivative, point):
        point = np.array(point)
        if len(point) != self._dim:
            raise ValueError("point should be of the same dimensions as"
                             " the problem. Problem is of dimension {:d} and "
                             "point has dimension {:d}.".format(
                                                         self._dim,
                                                         len(point),
                                                         ))
        pos = point - self.coords[:, 0]
        length = np.squeeze(np.diff(self.coords))
        pos = pos / length
        pos = pos * np.squeeze(np.diff(LIMITS)) + LIMITS[0]
        
        return self._space.getPointOperator(derivative, pos, self._jacobian)

    def getPhysCoordMesh(self):
        return self._phys_coords_mesh.reshape(-1, self._dim)
    
    def getPhysCoordVector(self, dim):
        return self._phys_coords_vector[dim].astype(float)

    def setSolvingSpace(self, space):
        self._space = space
        self._phys_coords_vector = np.array([self._space.getCoordsVector(dim, coord) for dim, coord in enumerate(self.coords)], dtype = object)
# The generation of the mesh list is a little bit cumbersome but not that hard.
# It needs a couple of steps.

        # First we generate a tuple where we shift the index order of the
        # standard generated numpy meshgrid grid.
        re_order = tuple(x for x in range(1, self._dim+1)) + (0,)
        
        # We generate the mesh with the numpy meshgrid routine and it is stored
        # in a list by default.
        self._phys_coords_mesh = np.meshgrid(
                                            *self._phys_coords_vector,
                                            indexing='ij',
                                            )
        
        # Then we accomodate the list into a numpy array for further
        # manipulation.
        self._phys_coords_mesh = np.array(self._phys_coords_mesh, dtype=float)
        
        # Transpose the mesh to obtain the desired order of axis.
        self._phys_coords_mesh = self._phys_coords_mesh.transpose(re_order)
        # Reshape the mesh to have the values in a list formatted way.
        self._phys_coords_mesh = self._phys_coords_mesh.reshape(-1, self._dim)
        
# This kind of vector allows us to obtain the correct coordinate for a given #
# quadrature point. For example, the rows of a mass matrix H will correspond #
# to the coordinate given by the same row in 'self._quad_coords_mesh'        #
# Thus, when using 'X = element.getPhysCoordMesh()[:, 0]' with a             #
# CartesianMeshElement class will give us the vector corresponding to the X  #
# coordinates in the same order than those found in the mass matrix or       #
# derivative operators.                                                      #
#                                                                   Julián   #

        self._weight_vector = [self._space.getWeightVector(n, x) 
                               for n, x in enumerate(self._jacobian)]
        self._weight_vector = np.array(
            self._weight_vector,
            dtype=object
            )
        self.A = np.zeros([np.prod(self._space._dof)*self._space.var_n]*2)
        self.F = np.zeros([np.prod(self._space._dof)*self._space.var_n])
        self._alpha = np.zeros(self._space._dof_n)
        self.vecZ = self._space.vecZ
        self.matZ = self._space.matZ
        self._idx = -np.ones(self._space._dof_n, dtype=int)

   # Part for solving the problem!
   
    def setSystemOp(self, K, G):
        """Sets the system differential equations.
        

        Parameters
        ----------
        K : numpy array of shape (quads x dofs)
            DESCRIPTION.
        G : numpy array of shape (quads)
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.K = K
        self.G = G
        W = self.getWeightMatrix()
        self.A += K.T @ W @ K
        self.F += K.T @ W @ G
        
    def setBoundaryOp(self, K, G, direction):
        """Sets a new differential problem for a given boundary direction.

        Parameters
        ----------
        K : TYPE
            DESCRIPTION.
        G : TYPE
            DESCRIPTION.
        direction : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        W = self.getWeightMatrixForDir(direction)
        self.A += K.T @ W @ K
        self.F += K.T @ W @ G
        
    def setPointOp(self, K, G):
        """Sets a new differential problem for a given point in the element

        Parameters
        ----------
        K : TYPE
            DESCRIPTION.
        G : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.A += K.T @ K
        self.F += K.T @ G
        
    def SolveSystem(self):
        """Solves the system.
        

        Returns
        -------
        None.

        """

        W = self.getWeightMatrix()

        # self._alpha, _ = cg(csr_matrix(self.A), self.F)
        self._alpha = np.linalg.solve(self.A, self.F)
        print("Condition number for matrix A is: {:e}".format(
            np.linalg.cond(np.linalg.inv(self.A))))
        R = self.K @ self._alpha - self.G
        self._residual = np.sqrt(R.T @ W @ R)
        
    def chrrruck(self):
        """Solves the system, Relloso's way.
        

        Returns
        -------
        None.

        """
        self.SolveSystem()
        
        
    def getValue(self, derivative, variable):
        H = self.getFieldOperator(derivative)
        alpha = self._alpha[
            variable * self._space._dof_per_var:
            variable * self._space._dof_per_var + self._space._dof_per_var
            ]
        return (H @ alpha)
    

if __name__ == "__main__":
    mve = MultiVariableElement(3, 1)
    mve.setBasisFunction(BasisFunction.Hermite)
    mve.setDof([4, 5, 6])
    mve.setQuadPoints([4, 5, 6])
    
    e = CartesianMeshElement(0, np.array([[0, 2], [-200, 200], [0, 1]]))
    e.setSolvingSpace(mve)
    
    # H = e.getFieldOperator([1, 0])
    # print(H)
    # dims = [4, 1]
    # elements = np.array([CartesianMeshElement(i, len(dims)) for i in range(np.prod(dims))]).reshape(*dims)
    # for counter in range(len(dims)):
    #     for e in elements[(slice(None), ) * counter + (0,) + (slice(None),) * (len(dims) - 1 - counter)].flat:
    #         e.boundaries[counter, 0] = True
    #     for e in elements[(slice(None), ) * counter + (-1,) + (slice(None),) * (len(dims) - 1 - counter)].flat:
    #         e.boundaries[counter, 1] = True
    # aux = CartesianMeshElement(0, 2, 2, [[0, 1], [0, 0.1]])
    # aux.setBasisFunction(BasisFunction.Hermite)
    # aux.setDof([4, 5])
    # aux.setQuadPoints([4, 5])

    # e = CartesianMeshElement(0, 2,4, [[0, 1],[0, 1]])
    # e.setBasisFunction(BasisFunction.Hermite)
    # e.setDof([8, 8])
    # e.setQuadPoints([30, 30])
    # H = e.getFieldOperator([0, 0])
    # Dx = e.getFieldOperator([1, 0])
    # Dxx = e.getFieldOperator([2, 0])
    # Dy = e.getFieldOperator([0, 1])
    # Dyy = e.getFieldOperator([0, 2])
    # tol = 1
    # old_res = 1
    # matZ = np.zeros(H.shape)
    # vecZ = np.zeros(np.prod(e._quad))
    
    # G = np.concatenate([vecZ, vecZ, vecZ, vecZ], axis = 0)
    
    # Bx0 = e.getBoundaryOperator(0, 0, [0, 0])
    # # x = 0
    # BC1 = np.concatenate([Bx0, matZ, matZ, matZ], axis = 1)
    # BC2 = np.concatenate([matZ, Bx0, matZ, matZ], axis = 1)
    # opZ = np.concatenate([matZ, matZ, matZ, matZ], axis = 1)
    # BC1 = np.concatenate([BC1, BC2, opZ, opZ], axis = 0)
    # Gx0 = np.zeros(G.shape)
    # indx0 = e.getBoundaryIndex(0, 0)
    # e.setBoundaryOp(BC1 *10, Gx0, 0, 0)
    
    # # x = 1 
    # Bx1 = e.getBoundaryOperator(0, 1, [0, 0])
    # BC1 = np.concatenate([Bx1, matZ, matZ, matZ], axis = 1)
    # BC2 = np.concatenate([matZ, Bx1, matZ, matZ], axis = 1)
    # opZ = np.concatenate([matZ, matZ, matZ, matZ], axis = 1)
    # BC1 = np.concatenate([BC1, BC2, opZ, opZ], axis = 0)
    # Gx1 = np.zeros(G.shape)
    # indx1 = e.getBoundaryIndex(0, 1)
    # e.setBoundaryOp(BC1*10, Gx1, 0, 1)
    
    # # y = 0
    # By0 = e.getBoundaryOperator(1, 0, [0, 0])
    # BC1 = np.concatenate([By0, matZ, matZ, matZ], axis = 1)
    # BC2 = np.concatenate([matZ, By0, matZ, matZ], axis = 1)
    # opZ = np.concatenate([matZ, matZ, matZ, matZ], axis = 1)
    # BC1 = np.concatenate([BC1, BC2, opZ, opZ], axis = 0)
    # Gy0 = np.zeros(G.shape)
    # indy0 = e.getBoundaryIndex(1, 0)
    # e.setBoundaryOp(BC1*10, Gy0, 1, 0)
    
    # # y = 1
    # By1 = e.getBoundaryOperator(1, 1, [0, 0])
    # BC1 = np.concatenate([By1, matZ, matZ, matZ], axis = 1)
    # BC2 = np.concatenate([matZ, By1, matZ, matZ], axis = 1)
    # opZ = np.concatenate([matZ, matZ, matZ, matZ], axis = 1)
    # BC1 = np.concatenate([BC1, BC2, opZ, opZ], axis = 0)
    # Gy1 = np.zeros(G.shape)
    # indy1 = e.getBoundaryIndex(1, 1)
    # Gy1[indy1] = np.ones(Gy1[indy1].shape)
    # e.setBoundaryOp(BC1*10, Gy1*10, 1, 1)

    # while tol > 5e-9:
    #     u = e.getValue([0, 0], 0)
    #     v = e.getValue([0, 0], 1)
    #     conv = u * Dx + v * Dy
    #     K1 = np.concatenate([conv, matZ, -(1/100)*Dy, Dx], axis = 1)
    #     K2 = np.concatenate([matZ, conv, Dx, Dy], axis = 1)
    #     K3 = np.concatenate([Dy, -Dx, H, matZ], axis = 1)
    #     K4 = np.concatenate([Dx, Dy, matZ, matZ], axis = 1)
        
    #     K = np.concatenate([K1, K2, K3, K4], axis = 0)
    #     e.setSystemOp(K, G)
    #     e.solveSystem()
    #     new_res = e._residual
    #     tol = np.abs(new_res - old_res)/new_res
    #     old_res = new_res
        
    #     print(tol)
