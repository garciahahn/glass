import os
import time
from mesh_element import MultiVariableElement
from mesh import CartesianMeshElement
from highorder import BasisFunction
import numpy as np
import matplotlib.pyplot as plt
#from matplotlib import cm
from numpy import pi, exp, cos, sin
#from mpl_toolkits.mplot3d import Axes3D
import types

class Case():
    def __init__(self, config, path):
        self._config = config
        self.__dict__.update(config)
        self.time = None
        self.path = path
        self.loop = types.MethodType(config["loop"], self)
    
    def RunLoop(self):
        self.errorL2 = np.empty(np.diff(self.iter))
        self.residual = np.empty(np.diff(self.iter))
        for i, d in enumerate(range(*self.iter)):
            print("Solving loop {:d} out of {:d}".format(i, self.errorL2.size))
            self.e, self.errorL2[i], self.residual[i] = self.loop(self._config, d)
        return self.e, self.errorL2, self.residual
    
    def PlotErrors(self):
        fig, ax = plt.subplots()
        test_dofs = np.array([x for x in range(*self.iter)])
        ax.scatter(
            np.array(test_dofs),
            self.errorL2,
            label="L2 error",
            s=10,
            marker='x',
            lw=0.5,
        )
        ax.scatter(
            np.array(test_dofs),
            self.residual,
            label="Residual",
            s=10,
            marker='+',
            lw=0.5,
        )
        ax.legend(loc='best')
        ax.set_yscale('log')
        ax.set_xscale('log')
        ax.set_xlabel("Degrees of Fredom (1D)")
        ax.set_ylabel("Residual - ErrorL2")
        ax.set_axisbelow(True)
        ax.grid(True, which='both', lw = 0.1)
        fig.savefig(self.path + self.name+"_rande.png", dpi = 300)
    
    def PlotLastState(self):
        if self.dim == 1:
            fig, ax = plt.subplots()
            X = self.e.getPhysCoordMesh()
            Y = self.e.getValue([0], 0)
            Y_an = self.f(X)

            ax.scatter(X, Y, label="Solved", s=6,
                       color='blue', marker='x', lw=0.5)
            ax.scatter(X, Y_an, label="Analytical", s=6,
                       color='red', marker='+', lw=0.3)

            ax.legend(loc='best')
            ax.grid(True, which='both', lw=0.2)
            ax.set_xlabel("X")
            ax.set_ylabel("Y")
            fig.savefig(self.path + self.name + "_laststate.png", dpi=300)
        elif self.dim == 2:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            X = self.e.getPhysCoordVector(0)
            Y = self.e.getPhysCoordVector(1)
            X, Y = np.meshgrid(X, Y)

            Z = self.e.getValue([0, 0], 0).reshape(X.shape[::-1]).T
            Z_an = self.f(X, Y)
            ax.plot_wireframe(X, Y, Z, label='Solved', color='blue', lw=0.5)
            ax.plot_wireframe(X, Y, Z_an, label='Analytical',
                              color='red', lw=0.5)
            ax.legend(loc='best')
            ax.grid(True, lw=0.2)
            ax.set_xlabel("X")
            ax.set_ylabel("Y")
            ax.set_zlabel("Z")
            fig.savefig(self.path + self.name + "_laststate.png", dpi=400)
        elif self.dim == 3:
            print("Plotting 3D figures lives only in the space of future ideas so far")
            # TODO : Plot 3D somehow, perhaps a slice through the center of the
            # main axis?

    def PrintConfig(self):
        for k, v in self._config.items():
            print("{}: {}".format(k, v))
def Gauss(x, y, mu, c):
    return np.exp(-( ( (x-mu[0])**2)/(2 * c[0]**2) + ((y-mu[1])**2)/(2* c[1] ** 2) ))
def Gaussx(x, y, mu, c):
    return -Gauss(x, y, mu, c) * (1/c[0]**2) * (x-mu[0])
def Gaussxx(x, y, mu, c):
    return Gauss(x, y, mu, c) * ( ((x-mu[0])/c[0]**2)**2 - (1/(c[0]**2)))
def Gaussy(x, y, mu, c):
    return -Gauss(x, y, mu, c) * (1/c[1]**2) * (y-mu[1])
def Gaussyy(x, y, mu, c):
    return Gauss(x, y, mu, c) * ( ((y-mu[1])/c[1]**2)**2 - (1/(c[1]**2)))


'''
First case scenario:
    f(x) = x * pi - cos(x * pi)
Differential equation:
    f' = pi * sin(pi * x) + pi
Domain:
    x = [-1, 1] (same as natural coordinates)
BC:
    f(-1) = - pi + 1
'''
def Case_001(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([d])
    mve.setQuadPoints([d])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)

    X = e.getPhysCoordMesh()[:, 0]
    D = e.getFieldOperator([1])
    
    K = D
    G = np.pi * np.sin(np.pi * X) + np.pi
    e.setSystemOp(K, G)
    
    Bx0 = e.getBoundaryOperator(0, 0, [0])
    Gbc = np.ones(G.shape) - np.pi
    
    e.setBoundaryOp(Bx0, Gbc, 0)
    
    e.SolveSystem()
    
    y_solved = e.getValue([0], 0)
    y_analytical = self.f(X)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error ** 2)) / y_solved.size
    residual = e._residual
    return e, errorL2, residual
analytical_001 = lambda x: np.pi * x - np.cos(np.pi * x)
config_case_001 = {"dim":1,
                   "iter": (4, 41),
                   "name": "Case_001",
                   "domain": [[-1, 1]],
                   "var": 1,
                   "basis": BasisFunction.Lagrange,
                   "f": analytical_001,
                   "loop": Case_001}

'''
Second case scenario:
    f(x) = e^-x + 1
Differential equation:
    f' + f = 1
Domain:
    x = [0, 3]
BC:
    f'(0) = - 1
'''
def Case_002(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([d])
    mve.setQuadPoints([d])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)

    X = e.getPhysCoordMesh()[:, 0]
    D = e.getFieldOperator([1])
    H = e.getFieldOperator([0])
    
    K = D + H
    G = np.ones(X.shape)
    e.setSystemOp(K, G)
    
    Bx0 = e.getBoundaryOperator(0, 0, [1])
    Gbc = -np.ones(G.shape)
    e.setBoundaryOp(Bx0, Gbc, 0)

    e.SolveSystem()
    
    y_solved = e.getValue([0], 0)
    y_analytical = self.f(X)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error ** 2)) / y_solved.size
    residual = e._residual
    return e, errorL2, residual

analytical_002 = lambda x: np.exp(-x) + 1
analytical_002d = lambda x: -np.exp(-x)
config_case_002 = {"dim":1,
                   "iter": (4, 41),
                   "name": "Case_002",
                   "domain": [[0, 3]],
                   "var": 1,
                   "basis": BasisFunction.Lagrange,
                   "f": analytical_002,
                   "fx": analytical_002d,
                   "loop": Case_002}

'''
Third case scenario:
    f(x) = sin(4*x)
Differential equation:
    f'' + f = -15 * sin(4*x)
Due to solving with Lagrangians (only first derivative plausible)
    g' + f = -15 * sin(4 * x)
    f' - g = 0
Domain:
    x = [-2*pi, pi]
BC:
    f(pi) = 0
    g(-2*pi) = 4
'''


def Case_003(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([d])
    mve.setQuadPoints([d])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)

    X = e.getPhysCoordMesh()[:, 0]
    D = e.getFieldOperator([1])
    H = e.getFieldOperator([0])

    K1 = np.hstack([H, D])
    K2 = np.hstack([D, -H])
    K = np.vstack([K1, K2])
    G = np.hstack([self.f(X)+self.fxx(X), e.vecZ])
    e.setSystemOp(K, G)

    Bx1 = e.getBoundaryOperator(0, 1, [0])
    BC1 = np.hstack([Bx1, e.matZ])
    BC2 = np.hstack([e.matZ, e.matZ])
    BC = np.vstack([BC1, BC2])
    Gbc = np.hstack([e.vecZ, e.vecZ])
    e.setBoundaryOp(BC, Gbc, 0)

    Bx0 = e.getBoundaryOperator(0, 0, [0])
    Bxd0 = e.getBoundaryOperator(0, 0, [1])
    BC1 = np.hstack([e.matZ, e.matZ])
    BC2 = np.hstack([e.matZ, Bx0])
    BC = np.vstack([BC1, BC2])
    Gbc = np.hstack([e.vecZ, np.ones(e.vecZ.shape)*4])
    e.setBoundaryOp(BC, Gbc, 0)

    e.SolveSystem()

    y_solved = e.getValue([0], 0)
    y_solved1 = e.getValue([0], 1)
    y_analytical = self.f(X)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error**2)) / y_solved.size
    residual = e._residual
    return e, errorL2, residual


def analytical_003(x): return np.sin(4*x)
def analytical_003d(x): return 4*np.cos(4*x)
def analytical_003dd(x): return -16*np.sin(4*x)


config_case_003 = {"dim": 1,
                   "iter": (4, 31),
                   "name": "Case_003",
                   "domain": [[-np.pi*2, np.pi]],
                   "var": 2,
                   "basis": BasisFunction.Lagrange,
                   "f": analytical_003,
                   "fx": analytical_003d,
                   "fxx": analytical_003dd,
                   "loop": Case_003}

'''
Fourth case scenario:
    f(x) = e^-x + 1
Differential equation:
    f'' + f = 2*e^-x + 1
Domain:
    x = [-2*pi, pi]
BC:
    f(pi) = e^-pi + 1
    f'(-2*pi) = e^(2*pi)
'''
def Case_004(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([d])
    mve.setQuadPoints([d])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)
        
    X = e.getPhysCoordMesh()[:, 0]
    DD = e.getFieldOperator([2])
    H = e.getFieldOperator([0])
    
    K = H + DD
    G = self.f(X) + self.fxx(X)
    e.setSystemOp(K, G)
    
    Bx1 = e.getBoundaryOperator(0, 1, [0])
    e.setBoundaryOp(Bx1, self.f(np.pi*np.ones_like(e.vecZ)), 0)
    
    Bx0 = e.getBoundaryOperator(0, 0, [1])
    e.setBoundaryOp(Bx0, self.fx(-2*np.pi*np.ones_like(e.vecZ)), 0)
    
    e.SolveSystem()
    
    y_solved = e.getValue([0], 0)
    y_analytical = self.f(X)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error ** 2)) / y_solved.size
    residual = e._residual
    return e, errorL2, residual

analytical_004 = lambda x: np.exp(-x) + 1
analytical_004d = lambda x: -np.exp(-x)
analytical_004dd = lambda x: np.exp(-x)
config_case_004 = {"dim":1,
                   "iter": (4, 10),
                   "name": "Case_004",
                   "domain": [[-np.pi*2, np.pi]],
                   "var": 1,
                   "basis": BasisFunction.Hermite,
                   "f": analytical_004,
                   "fx": analytical_004d,
                   "fxx": analytical_004dd,
                   "loop": Case_004}


'''
Fifth case scenario:
    f(x, y) = x/2 + y/4
Differential equation:
    df/dx + df/dy = 3/4
Domain:
    x = [0, 1]
    y = [-2, 0]
BC:
    f(x, 0) = x/2
    f(1, y) = 1/2 + y/4

Same quadrature point densit will be used
'''
def Case_005(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([d, 2*d])
    mve.setQuadPoints([d, 2*d])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)
        
    X = e.getPhysCoordMesh()[:, 0]
    Y = e.getPhysCoordMesh()[:, 1]
    Dx = e.getFieldOperator([1, 0])
    Dy = e.getFieldOperator([0, 1])
    H = e.getFieldOperator([0, 0])
    
    K = Dx + Dy
    G = self.fx(X, Y) + self.fy(X, Y)
    e.setSystemOp(K, G)
    
    # y = 0
    By0 = e.getBoundaryOperator(1, 1, [0, 0])
    Gbc = self.f(X, e.vecZ)
    BC = By0
    e.setBoundaryOp(BC, Gbc, 1)
    
    # x = 1        
    Bx1 = e.getBoundaryOperator(0, 1, [0, 0])
    BC = Bx1
    Gbc = self.f(np.ones_like(e.vecZ), Y)
    e.setBoundaryOp(BC, Gbc, 0)
    
    e.chrrruck()
    
    y_solved = e.getValue([0]*2, 0)
    y_analytical = self.f(X, Y)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error ** 2)) / y_solved.size
    residual = e._residual
    return e, errorL2, residual

analytical_005 = lambda x, y: x/2 + y/4
analytical_005x = lambda x, y: 1/2*np.ones_like(x)
analytical_005y = lambda x, y: 1/4*np.ones_like(y)
config_case_005 = {"dim":2,
                   "iter": (4, 31),
                   "name": "Case_005",
                   "domain": [[0,1], [-2,0]],
                   "var": 1,
                   "basis": BasisFunction.Lagrange,
                   "f": analytical_005,
                   "fx": analytical_005x,
                   "fy": analytical_005y,
                   "loop": Case_005}

'''
Sixth case scenario:
    f(x, y) = 2 * y * sin(4*pi*x)
    fx(x, y) = 8 * y * cos(4*np.pi*x)
    fxx(x, y) = -32* y * sin(4 * np.pi * x)
    fy(x, y) = 2 * sin(4*pi*x)
    fyy(x, y) = 0
Differential equation:
    d**2f/dx**2 + df/dy = -32 * pi**2 * y * sin(4 * pi * x) + 2 * np.sin(4*np.pi*x)
Due to solving with Lagrangians (only first derivative plausible)
    dg/dx + df/dy = -32 * pi**2 * y * sin(4 * pi * x)
    df/dx - g = 0
Domain:
    x = [-1, 2]
    y = [0, 1]
BC:
    f(x, 0) = 0
    f(2, y) = 0
    df/dx(-1, y) = 8 * pi * y
    df/dy(x, 1) = 2 * sin (4 * pi * x)
'''
def Case_006(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([3*d, d])
    mve.setQuadPoints([3*d, d])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)

    X = e.getPhysCoordMesh()[:, 0]
    Y = e.getPhysCoordMesh()[:, 1]
    Dx = e.getFieldOperator([1, 0])
    Dy = e.getFieldOperator([0, 1])
    H = e.getFieldOperator([0, 0])


    K1 = np.hstack([Dy, Dx])
    K2 = np.hstack([Dx, -H])
    K = np.vstack([K1, K2])
    G = np.hstack([self.fxx(X, Y) + self.fy(X, Y), e.vecZ])
    e.setSystemOp(K, G)
    
    # y = 0
    By0 = e.getBoundaryOperator(1, 0, [0, 0])
    BC1 = np.hstack([By0] + [e.matZ])
    BC2 = np.hstack([e.matZ] * 2)
    BC = np.vstack([BC1, BC2])
    Gbc = np.hstack([self.f(X, self.domain[1, 0])]+[e.vecZ])
    e.setBoundaryOp(BC, Gbc, 1)

    # x = 1        
    Bx1 = e.getBoundaryOperator(0, 1, [0, 0])
    BC1 = np.hstack([Bx1] + [e.matZ])
    BC = np.vstack([BC1, BC2])
    Gbc = np.hstack([self.f(self.domain[0, 1], Y)]+[e.vecZ])
    e.setBoundaryOp(BC, Gbc, 0)
    
    # x = 0
    Bx0 = e.getBoundaryOperator(0, 0, [0, 0])
    BC1 = np.hstack([Bx0] + [e.matZ])
    BC = np.vstack([BC1, BC2])
    Gbc = np.hstack([self.f(self.domain[0, 0], Y)] + [e.vecZ])
    e.setBoundaryOp(BC, Gbc, 0)
    
    y = 1
    By1 = e.getBoundaryOperator(1, 1, [0, 1])
    BC1 = np.hstack([By1] + [e.matZ])
    BC = np.vstack([BC1, BC2])
    Gbc = np.hstack([self.fy(X, self.domain[1, 1])] + [e.vecZ])
    e.setBoundaryOp(BC, Gbc, 1)
    
    e.chrrruck()
    
    y_solved = e.getValue([0]*2, 0)
    y_analytical = self.f(X, Y)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error ** 2))/ y_solved.size
    residual = e._residual

    return e, errorL2, residual

analytical_006 = lambda x, y: 2 * y * np.sin(4*np.pi*x)
analytical_006x = lambda x, y: 8 * np.pi * y * np.cos(4*np.pi*x)
analytical_006xx = lambda x, y: -32* y *  np.pi ** 2 * np.sin(4 * np.pi * x)
analytical_006y = lambda x, y: 2 * np.sin(4*np.pi*x)
analytical_006yy = lambda x, y: np.zeros_like(x)

config_case_006 = {"dim":2,
                   "iter": (4, 21),
                   "name": "Case_006",
                   "domain": np.array([[-2,1], [0,1]]),
                   "var": 2,
                   "basis": BasisFunction.Lagrange,
                   "f": analytical_006,
                   "fx": analytical_006x,
                   "fxx": analytical_006xx,
                   "fy": analytical_006y,
                   "fyy": analytical_006yy,
                   "loop": Case_006}

'''
Seventh case scenario:
    f(x, y) = Gauss(x, y) = np.exp(-((x-mu_x)**2 / (2*c_x**2) + (y-mu_y)**2 / (2*c_y**2)))
Differential equation:
    d**2 f/x**2 + d**2 f /y**2 = Gauss(x, y) * ((((x-mu_x)/c_x**2)-(1/c_x**2)) + (((y-mu_y)/c_y**2)-(1/c_y**2)))
Due to solving with Lagrangians (only first derivative plausible)
    dg/dx + dh/dy = source(x, y)
    df/dx - g = 0
    df/dy - h = 0
Domain:
    x = [-2, 1]
    y = [0, 5]
BC:
    f(x, 0) = Gauss(x, 0)
    df/dy(1, y) = - Gauss(1, y) * (y-mu_y) / c_y**2
    f(-2, y) = Gauss(-2, y)
    df/dy(x, 5) = - Gauss(x, 5) * (y-mu_y) / c_y**2
'''


def Case_007(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([d, d])
    mve.setQuadPoints([d, d])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)

    X = e.getPhysCoordMesh()[:, 0]
    Y = e.getPhysCoordMesh()[:, 1]
    Dx = e.getFieldOperator([1, 0])
    Dy = e.getFieldOperator([0, 1])
    H = e.getFieldOperator([0, 0])


    K1 = np.hstack([e.matZ, Dx, Dy])
    K2 = np.hstack([Dx, -H, e.matZ])
    K3 = np.hstack([Dy, e.matZ, -H])
    K = np.vstack([K1, K2, K3])
    G = np.hstack([self.fxx(X, Y) + self.fyy(X, Y)] + [e.vecZ]*(self.var-1))
    e.setSystemOp(K, G)
    
    # y = bottom
    By0 = e.getBoundaryOperator(1, 0, [0, 0])
    BC1 = np.hstack([By0] + [e.matZ]*(self.var-1))
    BC2 = np.hstack([e.matZ] * (self.var))
    BC = np.vstack([BC1] + [BC2]* (self.var-1))
    Gbc = np.hstack([self.f(X, self.domain[1, 0])]+[e.vecZ]*(self.var-1))
    e.setBoundaryOp(BC, Gbc, 1)
    
    # y = top
    By1 = e.getBoundaryOperator(1, 1, [0, 1])
    BC1 = np.hstack([By1] + [e.matZ]*(self.var-1))
    BC = np.vstack([BC1] + [BC2]* (self.var-1))
    Gbc = np.hstack([self.fy(X, self.domain[1, 1])]+[e.vecZ]*(self.var-1))
    e.setBoundaryOp(BC, Gbc, 1)

    # x = top
    Bx1 = e.getBoundaryOperator(0, 1, [0, 1])
    BC1 = np.hstack([Bx1] + [e.matZ]*(self.var-1))
    BC = np.vstack([BC1] + [BC2]* (self.var-1))
    Gbc = np.hstack([self.fy(self.domain[0, 1], Y)]+[e.vecZ]*(self.var-1))
    e.setBoundaryOp(BC, Gbc, 0)
    
     # x = bottom
    Bx0 = e.getBoundaryOperator(0, 0, [0, 0])
    BC1 = np.hstack([Bx0] + [e.matZ]*(self.var-1))
    BC = np.vstack([BC1] + [BC2]* (self.var-1))
    Gbc = np.hstack([self.f(self.domain[0, 0], Y)]+[e.vecZ]*(self.var-1))
    e.setBoundaryOp(BC, Gbc, 0)

    e.chrrruck()
    
    y_solved = e.getValue([0]*2, 0)
    y_analytical = self.f(X, Y)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error ** 2))/ y_solved.size
    residual = e._residual

    return e, errorL2, residual
mu = np.array([0, 3])
c = np.array([1, 2])/3
analytical_007 = lambda x, y: Gauss(x, y, mu, c)
analytical_007x = lambda x, y: Gaussx(x, y, mu, c)
analytical_007xx = lambda x, y: Gaussxx(x, y, mu, c)
analytical_007y = lambda x, y: Gaussy(x, y, mu, c)
analytical_007yy = lambda x, y: Gaussyy(x, y, mu, c)

config_case_007 = {"dim":2,
                   "iter": (3, 26),
                   "name": "Case_007",
                   "domain": np.array([[-2,1], [0,5]]),
                   "var": 3,
                   "basis": BasisFunction.Lagrange,
                   "f": analytical_007,
                   "fx": analytical_007x,
                   "fxx": analytical_007xx,
                   "fy": analytical_007y,
                   "fyy": analytical_007yy,
                   "loop": Case_007}

'''
Eighth case scenario:
    f(x, y) = Gauss(x, y) = np.exp(-((x-mu_x)**2 / (2*c_x**2) + (y-mu_y)**2 / (2*c_y**2)))
Differential equation:
    d**2 f/x**2 + d**2 f /y**2 = Gauss(x, y) * ((((x-mu_x)/c_x**2)-(1/c_x**2)) + (((y-mu_y)/c_y**2)-(1/c_y**2)))
Domain:
    x = [-2, 1]
    y = [0, 5]
BC:
    f(x, 0) = Gauss(x, 0)
    df/dy(1, y) = - Gauss(1, y) * (y-mu_y) / c_y**2
    f(-2, y) = Gauss(-2, y)
    df/dy(x, 5) = - Gauss(x, 5) * (y-mu_y) / c_y**2
'''

def Case_008(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([d, d])
    mve.setQuadPoints([d, d])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)

    X = e.getPhysCoordMesh()[:, 0]
    Y = e.getPhysCoordMesh()[:, 1]
    Dxx = e.getFieldOperator([2, 0])
    Dyy = e.getFieldOperator([0, 2])
    H = e.getFieldOperator([0, 0])

    K = Dxx + Dyy
    G = self.fxx(X, Y) + self.fyy(X, Y)
    e.setSystemOp(K, G)
    
    # y = bottom
    By0 = e.getBoundaryOperator(1, 0, [0, 0])
    Gbc = self.f(X, self.domain[1, 0])
    e.setBoundaryOp(By0, Gbc, 1)
    
    # y = top
    By1 = e.getBoundaryOperator(1, 1, [0, 1])
    Gbc = self.fy(X, self.domain[1, 1])
    e.setBoundaryOp(By1, Gbc, 1)

    # x = top
    Bx1 = e.getBoundaryOperator(0, 1, [0, 1])
    Gbc = self.fy(self.domain[0, 1], Y)
    e.setBoundaryOp(Bx1, Gbc, 0)
    
     # x = bottom
    Bx0 = e.getBoundaryOperator(0, 0, [0, 0])
    Gbc = self.f(self.domain[0, 0], Y)
    e.setBoundaryOp(Bx0, Gbc, 0)

    e.chrrruck()
    
    y_solved = e.getValue([0]*2, 0)
    y_analytical = self.f(X, Y)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error ** 2))/ y_solved.size
    residual = e._residual

    return e, errorL2, residual
mu = np.array([0, 3])
c = np.array([1, 2])

config_case_008 = {"dim":2,
                   "iter": (4, 10),
                   "name": "Case_008",
                   "domain": np.array([[-2,1], [0,5]]),
                   "var": 1,
                   "basis": BasisFunction.Hermite,
                   "f": analytical_007,
                   "fx": analytical_007x,
                   "fxx": analytical_007xx,
                   "fy": analytical_007y,
                   "fyy": analytical_007yy,
                   "loop": Case_008}

# '''
# Ninth case scenario:
#     f(x, y, z) = np.exp(-2*x) * np.exp(y) * np.exp(-z)
# Differential equation:
#     df/dx + df/dy + df/dz = -2 * np.exp(-2*x) * np.exp(y) * np.exp(-z)
#                            + np.exp(-2*x) * np.exp(y) * np.exp(-z)
#                            - np.exp(-2*x) * np.exp(y) * np.exp(-z)
# Domain:
#     x = [-2, 1]
#     y = [0, 5]
#     z = [-1, 2]
# BC:
#     f(0, y, z) = np.exp(y) * np.exp(-z)
#     f(x, 0, z) = np.exp(-2*x) * np.exp(-z)
#     f(x, y, 0) = np.exp(-2*x) * np.exp(y)
#     
# '''

def Case_009(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([d, d+1, d-1])
    mve.setQuadPoints([d, d+1, d-1])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)

    X = e.getPhysCoordMesh()[:, 0]
    Y = e.getPhysCoordMesh()[:, 1]
    Z = e.getPhysCoordMesh()[:, 2]
    Dx = e.getFieldOperator([1, 0, 0])
    Dy = e.getFieldOperator([0, 1, 0])
    Dz = e.getFieldOperator([0, 0, 1])
    H = e.getFieldOperator([0, 0, 0])

    K = Dx + Dy + Dz
    G = self.fx(X, Y, Z) + self.fy(X, Y, Z) + self.fz(X, Y, Z)
    e.setSystemOp(K, G)
    
    
    # x = bottom
    BC = e.getBoundaryOperator(0, 0, [0, 0, 0])
    Gbc = self.f(self.domain[0, 0], Y, Z)
    e.setBoundaryOp(BC, Gbc, 0)
    # x = top
    # e.setBoundaryOp(BC, Gbc, 0)
    # y = bottom
    BC = e.getBoundaryOperator(1, 0, [0, 0, 0])
    Gbc = self.f(X, self.domain[1, 0], Z)
    e.setBoundaryOp(BC, Gbc, 1)
    # y = top
    # e.setBoundaryOp(BC, Gbc, 1)
    # z = bottom
    BC = e.getBoundaryOperator(2, 0, [0, 0, 0])
    Gbc = self.f(X, Y, self.domain[2, 0])
    e.setBoundaryOp(BC, Gbc, 2)
    # z = top
    # e.setBoundaryOp(BC, Gbc, 2)
    
    e.chrrruck()
    
    y_solved = e.getValue([0]*self.dim, 0)
    y_analytical = self.f(X, Y, Z)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error ** 2))/ y_solved.size
    residual = e._residual

    return e, errorL2, residual
analytical_009 = lambda x, y, z: np.exp(-2*x) * np.exp(y) * np.exp(-z)
analytical_009x = lambda x, y, z: -2 * np.exp(-2*x) * np.exp(y) * np.exp(-z)
analytical_009y = lambda x, y, z: np.exp(-2*x) * np.exp(y) * np.exp(-z)
analytical_009z = lambda x, y, z: - np.exp(-2*x) * np.exp(y) * np.exp(-z)

config_case_009 = {"dim":3,
                   "iter": (3, 19),
                   "name": "Case_009",
                   "domain": np.array([[-2,1], [0,5], [-1, 2]]),
                   "var": 1,
                   "basis": BasisFunction.Lagrange,
                   "f": analytical_009,
                   "fx": analytical_009x,
                   "fy": analytical_009y,
                   "fz": analytical_009z,
                   "loop": Case_009}

'''
Tenth case scenario:
    f(x, y, z) = 2 * y * sin(pi*x) * np.exp(0.5 * (z + 0.2) * np.sin(1.5 * np.pi * z) )
Differential equation:
    g(z) = ((z+0.2)/2) * np.sin(1.5*np.pi*z)
    d**2f/dx**2 + df/dy + df/dz = - 2 * np.pi**2 * y * np.sin(np.pi * x) * np.exp(g(z))
        + 2 * np.sin(np.pi * x) * np.exp(g(z))
        + 2 * y * np.sin(np.pi * x) * np.exp(g(z)) * (np.sin(1.5*np.pi*z)/2 + 
                                                      0.75 * np.pi * np.cos(np.pi * 1.5 * z) *
                                                      (z+0.2))
Due to solving with Lagrangians (only first derivative plausible)
    dg/dx + df/dy + df/dz = source(x, y, z)
    df/dx - g = 0
Domain:
    x = [0, 1]
    y = [-1, 1]
    z = [0, 2]
BC:
    f(x, bottom, z) = 
    f(top, y, z) = 
    df/dx(bottom, y) = 
    df/dz(x, y, top) = 
'''
def Case_010(self, config, d):
    mve = MultiVariableElement(self.dim, self.var)
    mve.setBasisFunction(self.basis)
    mve.setDof([d, np.max([2, d//3]), d*2])
    mve.setQuadPoints([d, np.max([2, d//3]), d*2])

    e = CartesianMeshElement(0, self.domain)
    e.setSolvingSpace(mve)

    X = e.getPhysCoordMesh()[:, 0]
    Y = e.getPhysCoordMesh()[:, 1]
    Z = e.getPhysCoordMesh()[:, 2]
    Dx = e.getFieldOperator([1, 0, 0])
    Dy = e.getFieldOperator([0, 1, 0])
    Dz = e.getFieldOperator([0, 0, 1])
    H = e.getFieldOperator([0, 0, 0])


    K1 = np.hstack((Dy + Dz, Dx))
    K2 = np.hstack((Dx, -H))
    K = np.vstack((K1, K2))
    G = np.hstack((self.fxx(X, Y, Z) + self.fy(X, Y, Z) + self.fz(X, Y, Z), e.vecZ))
    e.setSystemOp(K, G)
    
    
    # x = bottom
    BC = e.getBoundaryOperator(0, 0, [1, 0, 0])
    BC1 = np.hstack((BC, e.matZ))
    BC_empty = np.hstack((e.matZ,)*self.var)
    BC = np.vstack((BC1, BC_empty))
    Gbc = np.hstack((self.fx(self.domain[0, 0], Y, Z), e.vecZ))
    e.setBoundaryOp(BC, Gbc, 0)
    BC = e.getBoundaryOperator(0, 0, [0, 0, 0])
    BC1 = np.hstack((BC, e.matZ))
    BC = np.vstack((BC1, BC_empty))
    Gbc = np.hstack((self.f(self.domain[0, 0], Y, Z), e.vecZ))
    e.setBoundaryOp(BC, Gbc, 0)
    # x = top
    BC = e.getBoundaryOperator(0, 1, [0, 0, 0])
    BC1 = np.hstack((BC, e.matZ))
    BC = np.vstack((BC1, BC_empty))
    Gbc = np.hstack((self.f(self.domain[0, 1], Y, Z), e.vecZ))
    e.setBoundaryOp(BC, Gbc, 0)
    
    # e.setBoundaryOp(BC, Gbc, 0)
    # y = bottom
    BC = e.getBoundaryOperator(1, 0, [0, 0, 0])
    BC1 = np.hstack((BC, e.matZ))
    BC = np.vstack((BC1, BC_empty))
    Gbc = np.hstack((self.f(X, self.domain[1, 0], Z), e.vecZ))
    e.setBoundaryOp(BC, Gbc, 1)
    # y = top
    # e.setBoundaryOp(BC, Gbc, 1)
    # z = bottom
    BC = e.getBoundaryOperator(2, 0, [0, 0, 0])
    BC1 = np.hstack((BC, e.matZ))
    BC = np.vstack((BC1, BC_empty))
    Gbc = np.hstack((self.f(X, Y, self.domain[2, 0]), e.vecZ))
    e.setBoundaryOp(BC, Gbc, 2)
    BC = e.getBoundaryOperator(2, 0, [0, 0, 1])
    BC1 = np.hstack((BC, e.matZ))
    BC = np.vstack((BC1, BC_empty))
    Gbc = np.hstack((self.fz(X, Y, self.domain[2, 0]), e.vecZ))
    e.setBoundaryOp(BC, Gbc, 2)
    # z = top
    BC = e.getBoundaryOperator(2, 1, [0, 0, 0])
    BC1 = np.hstack((BC, e.matZ))
    BC = np.vstack((BC1, BC_empty))
    Gbc = np.hstack((self.f(X, Y, self.domain[2, 1]), e.vecZ))
    e.setBoundaryOp(BC, Gbc, 2)
    
    e.chrrruck()
    
    y_solved = e.getValue([0]*self.dim, 0)
    y_analytical = self.f(X, Y, Z)
    error = y_solved - y_analytical
    errorL2 = np.sqrt(np.sum(error ** 2))/ y_solved.size
    residual = e._residual

    return e, errorL2, residual

aux_g = lambda z: 0.5 * (z + 0.2) * np.sin(1.5 * np.pi * z)
aux_g_prima = lambda z: 0.5 * np.sin(1.5 * np.pi * z) + 0.75 * np.pi * np.cos(1.5 * np.pi * z) * (z + 0.2)
analytical_010 = lambda x, y, z: 2 * y * np.sin(np.pi * x) * np.exp(aux_g(z))
analytical_010x = lambda x, y, z: np.pi * y * 2 * np.cos(np.pi*x)*np.exp(aux_g(z))
analytical_010xx = lambda x, y, z: -2 * np.pi**2 *y * np.sin(np.pi*x) * np.exp(aux_g(z))
analytical_010y = lambda x, y, z: 2 * np.sin(np.pi * x) * np.exp(aux_g(z))
analytical_010z = lambda x, y, z: 2 * y * np.sin(np.pi * x) * np.exp(aux_g(z))\
    * aux_g_prima(z)

config_case_010 = {"dim":3,
                   "iter": (2, 15),
                   "name": "Case_010",
                   "domain": np.array([[0,1], [-1,1], [0, 2]]),
                   "var": 2,
                   "basis": BasisFunction.Lagrange,
                   "f": analytical_010,
                   "fx": analytical_010x,
                   "fxx": analytical_010xx,
                   "fy": analytical_010y,
                   "fz": analytical_010z,
                   "loop": Case_010}


def PrintListCase(list_case, point_to = None):
    for case in list_case:
        if point_to is not case:
            print("     ", case["name"])
        else:
            print("---> ", case["name"])

def runAllCases():
    test_functions = [
        # config_case_001,
        # config_case_002,
        # config_case_003,
        # config_case_004,
        # config_case_005,
        config_case_006,
        # config_case_007,
        # config_case_008,
        # config_case_009,
        # config_case_010,
        ]
            
    # Creating directory to store images and results
    # (no more non-stored images! :D)
    test_dir = "../test/test"
    test_dir += time.strftime("_%y%m%d%H%M%S/", time.localtime())
    if not os.path.isdir(test_dir):
        os.mkdir(test_dir)
        
    # Loop running all the cases
    
    # Print the list of cases
    print("Running the following cases:")
    PrintListCase(test_functions)
    
    # Start taking time to know where are we
    start_t = time.time()
    
    
    
    # The loop itself
    for case in test_functions:
    # Print which case we are running
        print("Now running: ")
        PrintListCase(test_functions, case)
        c = Case(case, test_dir)
        e, err, res = c.RunLoop()
        c.PlotErrors()
        c.PlotLastState()

if __name__ == "__main__":
    runAllCases()
