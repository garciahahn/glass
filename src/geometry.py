#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 14:52:03 2021

@author: jnhahn
"""

''' ***Pseudo Coding***
- Find elements of the dimension of the problem.
    paths = np.array([x for x in itertools.permutations([i for i in range(dim)])])
    (should get something like [[0, 1, 2], [1, 0, 2], [0, 2, 1]...etc])
    connected = np.zeros(paths.size, dtype = bool)
    for n, coords in enumerate(bottom_corners):
        for path in paths:
            if P.find(path) is not None:
                connected[n] = True
            else:
                connected[n] = False
                break
        if all path is True:
            create object with the +1 oriented points
            objects_points_coords = all combinations of ([0, 0, 1], [0, 1, 0], ) (np.array([x for x in itertools.product(range(2), repeat = dim)]))
            object_points = [P.find(object_coord + coords) for coords in objects_points_coords]
            create a n dimensional vector of dimensions (2 x 2 x 2 x ...) where the points are referenced as [0, 0, 1], [0, 0, 0], etc
            create object from vector:
                store points
                store dimension
                Give it an ID
        else:
            pass
Square:
    points
    ID
    dimension
    root (bottom point)
    
    CheckIntegrity
    FindSquare
Geometry:
    addPoints
    addSquares
    
    FindSquares
    DisconnectPoints
    UpdateSquares(through points?)
Mesh:
    addElements
- Create an geometric object that takes all the points that integrates it.
- Assign mesh element objects to the individual geometric elements.
- Create a weight vector for the entire problem
- link weight vectors of the mesh elements to those of the entire problem using some algorithm that orders them.
- Find the boundaries geometric elements.
- Assign the dynamic equations for the problem.
- Construct the bigger matrix A (and F).
- Start solving.
'''

import numpy as np
import matplotlib.pyplot as plt
from itertools import product, permutations
from mpl_toolkits.mplot3d.proj3d import proj_transform
from matplotlib.text import Annotation
from mesh_element import *
from time import time
from scipy.sparse.linalg import cg
from scipy.sparse import csr_matrix
import scipy

class Annotation3D(Annotation):
    '''Annotate the point xyz with text s'''

    def __init__(self, s, xyz, *args, **kwargs):
        Annotation.__init__(self,s, xy=(0,0), *args, **kwargs)
        self._verts3d = xyz        

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.xy=(xs,ys)
        Annotation.draw(self, renderer)

def annotate3D(ax, s, *args, **kwargs):
    '''add anotation text s to to Axes3d ax'''

    tag = Annotation3D(s, *args, **kwargs)
    ax.add_artist(tag)

class Point():
    def __init__(self, coords, ID):
        self.coords = np.array(coords)
        self.ID = ID
        self._dim = len(self.coords)
        self._linked = np.empty([self._dim, 2], dtype = object)
        self._belongs = np.empty(2 ** self._dim, dtype = object)
    
    def ConnectTo(self, point):
        delta = point.coords - self.coords
        np.round(delta, decimals = 6, out = delta)
        if np.count_nonzero(delta) == 1:
            direction = np.where(delta != 0)
            sense = 1 if delta[direction] > 0 else 0
            self._linked[direction, sense] = point
            point._linked[direction, np.bitwise_not(sense)] = self
        else:
            print("Not possible to connect in a cross direction")
    
    def FindByPath(self, path, sense):
        path = np.array([path]).reshape(-1)
        sense = np.array([sense]).reshape(-1)
        ng = self._getNeighbor(path[0], sense[0])
        if len(path) == 1 or ng is None:
            return ng
        else:
            return ng.FindByPath(path[1:], sense[1:])
    
    def _getNeighbor(self, direction, sense):
        sense = 1 if sense > 0 else 0
        edg = self._linked[direction, sense]
        if edg is not None:
            return self._linked[direction, sense]
        else:
            return None
    def __str__(self):
        return "P:{:d}, coord: {}".format(self.ID, str(self.coords))
    
    def __repr__(self):
        return self.__str__()

class CartesianGeometricalElement(CartesianMeshElement):
    def __init__(self, ID, points):
        self._points = points
        self._root = points.flat[0]
        for p in self._points.flat:
            p._belongs[np.argmax(p._belongs == None)] = self
        super().__init__(ID, self.getCoordinates())
        self._linked = np.empty([self._dim, 2], dtype=object)
        self._cont = None
        self._equation = None
       
    def ConnectTo(self, element):
        # FIXME: this should be done differently, with inheritance from a
        # common class for point and CartesianGeometricalElement
        delta = element._root.coords - self._root.coords
        np.round(delta, decimals=6, out=delta)
        if np.count_nonzero(delta) == 1:
            direction = np.where(delta != 0)
            sense = 1 if delta[direction] > 0 else 0
            self._linked[direction, sense] = element
            element._linked[direction, np.bitwise_not(sense)] = self
        else:
            raise ValueError('element to connect to is in a cross direction.') 
    
    def setContinuity(self, c):
        c = np.array(c)
        if not len(c) == self._dim:
            raise ValueError(
                "Continuity vector must be of the same dimensions as the"
                " element."
                )
        
        if not np.issubdtype(c.dtype, np.integer) \
            or np.any(c < 0):
            raise ValueError(
                "Continuity  must be a numeric integer greater or equal than 0.\n"
                'Argument passed was: {:}'.format(str(c))
                )
        
        if self._space._basis_function is BasisFunction.Lagrange \
            and np.any(c > 0):
            raise ValueError('Lagrangian basis only support C0 elements.\n'
                             'Argument passed was: {:}'.format(str(c)))
        
        if self._space._basis_function is BasisFunction.Hermite \
            and np.any(c > 1):
            raise ValueError('Hermitian basis only support C1 elements.\n'
                             'Argument passed was: {:}'.format(str(c)))

        self._cont = c
        
    def getContinuity(self, direction):
        if not isinstance(direction, int):
            raise ValueError('\'direction\' must be an integer.')
        
        if direction < 0 or direction >= self._dim:
            raise ValueError("direction parameter out of range.")
        
        return self._cont[direction]
    
    def getCoordinates(self):
        return np.vstack([self._points.flatten()[0].coords, self._points.flatten()[-1].coords]).T
    
    def IntegrateEquations(self, global_alpha, config=None):
        return self._equation(self, global_alpha, config)

    def getValue(self, derivative, variable, global_alpha):
        H = self.getFieldOperator(derivative)
        idx = self._idx[
            variable * self._space._dof_per_var:
            variable * self._space._dof_per_var + self._space._dof_per_var
            ]
        return (H @ global_alpha[idx])
    
    def getResidual(self, global_alpha):
        R = self.K @ global_alpha[self._idx] - self.G
        W = self.getWeightMatrix()
        
        return R.T @ W @ R
    
    def __str__(self):
        return "ID:{:d}, {:d} dim, coords: {:s}".format(self.ID, self._dim, str(self.getCoordinates()))
    
    def __repr__(self):
        return self.__str__()

class CartesianGeometry():
    def __init__(self, initial_vectors):
        self._coord_vectors = initial_vectors
        self._point_n = np.array([len(x) for x in initial_vectors])
        self._dim = len(self._coord_vectors)
        self._element_n = np.array([v.size - 1 for v in self._coord_vectors])
        self._meshgrid = np.array(np.meshgrid(
            *self._coord_vectors, indexing='ij'),
            dtype=float)
        self._points = np.empty_like(self._meshgrid[0], dtype = object)
        # Create all the points in the cartesian geometry
        for n, counter in enumerate(product(*[range(len(x)) for x in self._coord_vectors])):
            self._points[counter] = Point(self._meshgrid[(slice(None),) + counter], n)
            
        # Create edges in the geometry using the points and the +1 relation
        # This operation should be independent of dimension
        print("Linking all points...")
        for n, counter in enumerate(product(*[range(x) for x in self._point_n])):
            for i in range(self._dim):
                idx = list(counter)
                idx[i] += 1
                if idx[i] < self._point_n[i]:
                    self._points[counter].ConnectTo(self._points[tuple(idx)])
                
        # Create n dimensional elements being n the dimension of the problem
        print("Creating elements...")
        self._pts_idx = np.array([x for x in np.ndindex(self._points.shape)])
        self._pts_with_sqrs = np.array([FindSquare(p) for p in self._points.flat],
                                       dtype = bool).reshape(self._points.shape)
        self._squares = np.array([CartesianGeometricalElement(n,
             self._points[tuple(slice(start, start + 2) for start in root)]) for n, root in
                  enumerate(self._pts_idx[np.where(self._pts_with_sqrs.flat)])])
        self._squares = self._squares.reshape(self._point_n-1)
    
        print("Linking elements...")
        for counter in np.ndindex(self._squares.shape):
            for i in range(self._dim):
                idx = list(counter)
                idx[i] += 1
                if idx[i] < self._squares.shape[i]:
                    self._squares[counter].ConnectTo(self._squares[tuple(idx)])
    
    def Plot(self):
        if self._dim == 1:
            fig, ax = plt.subplots()
            ax.scatter(self._coord_vectors[0], np.zeros_like(self._coord_vectors[0]), s = 5)
            for p in self._points.flat:
                ax.annotate("{:s}".format(str(p.ID)), (*p.coords, 0), fontsize = 3,
                     xytext=(10, 10), textcoords='offset pixels', color='red')
            for counter in np.ndindex(self._points.shape):
                for i in range(self._dim):
                    idx = list(counter)
                    idx[i] += 1
                    if idx[i] < self._point_n[i]:
                        x = np.array([self._points[counter].coords, self._points[tuple (idx)].coords])
                        y = np.zeros_like(x)
                        ax.plot(x, y, color='royalblue', alpha = 0.5)
            for sq in self._squares.flat:
                ax.annotate("{:s}".format(str(sq.ID)), (sq.getCoordinates().mean(), 0), fontsize = 4,
                            ha='center', xytext=(0, 10), textcoords='offset pixels', color = 'green')
        elif self._dim == 2:
            fig, ax = plt.subplots()
            x, y = np.split(self._meshgrid, 2, 0)
            ax.scatter(x, y, s = 5)
            for p in self._points.flat:
                ax.annotate("{:s}".format(str(p.ID)), p.coords, fontsize = 3, xytext=(10, 10),
                            textcoords='offset pixels', color='red')
            for n, counter in enumerate(product(*[range(x) for x in self._point_n])):
                for i in range(self._dim):
                    idx = list(counter)
                    idx[i] += 1
                    if idx[i] < self._point_n[i]:
                        x, y = np.array([self._points[counter].coords, self._points[tuple(idx)].coords]).T
                        ax.plot(x, y, color='royalblue', alpha = 0.5)
            for sq in self._squares.flat:
                ax.annotate("{:s}".format(str(sq.ID)), sq.getCoordinates().mean(axis=1),
                            fontsize = 4, ha="center", va="center", color='green')
                
        elif self._dim == 3:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
            x, y, z = np.split(self._meshgrid, 3, 0)
            ax.grid(False)
            ax.scatter(x, y, z, s=10)
            kwargs = {"alpha": 0.5, 'color' : 'royalblue', "linewidth": 0.5}
            
            for p in self._points.flat:
                annotate3D(
                           ax,
                           "{:s}".format(str(p.ID)),
                           p.coords,
                           fontsize=6,
                           xytext=(10, 10,),
                           textcoords='offset pixels',
                           color='red',
                           )
            for n, counter in enumerate(product(*[range(x) for x in self._point_n])):
                for i in range(self._dim):
                    idx = list(counter)
                    idx[i] += 1
                    if idx[i] < self._point_n[i]:
                        x, y, z = np.array([
                            self._points[counter].coords,
                            self._points[tuple(idx)].coords]
                            ).T
                        ax.plot(x, y, z, color='royalblue', alpha = 0.5)
            ax.set_xlabel("X")
            ax.set_ylabel("Y")
            ax.set_zlabel("Z")
            plt.show()

class GlobalFiniteElement(CartesianGeometry):
    def __init__(self, initial_vectors):
        super().__init__(initial_vectors)
        self._dof_n = None
        self.A = None
        self.F = None
        self._alpha = None
        self.ax = None
        self.fig = None
        
    def setContinuityForAll(self, c):
        for sq in self._squares.flat:
            sq.setContinuity(c)
    
    def setSolvingSpaceForAll(self, space):
        for sq in self._squares.flat:
            sq.setSolvingSpace(space)
    
    def setEquationForAll(self, eq):
        for sq in self._squares.flat:
            sq._equation = eq
    
    def _AssignDof(self):
        self._dof_n = self._getDofNumber()
        self._idx = np.arange(self._dof_n, dtype=int)
        self._alpha = np.zeros(self._dof_n)
        self.A = np.zeros((self._dof_n, self._dof_n))
        self.F = np.zeros_like(self._alpha)
        last = 0
        for counter, sq in np.ndenumerate(self._squares):
            idx_aux = np.ones(sq._space._dof_n, dtype=bool)
            for i in range(self._dim):
                if counter[i] - 1 >= 0:
                    neighbor = sq._linked[i, 0]
                    c = np.min([neighbor.getContinuity(i), sq.getContinuity(i)])
                    for j in range(c+1):
                        current_idx = sq.getContinuityDofs(i, -1, j)
                        neighbor_idx = neighbor.getContinuityDofs(i, 1, j)
                        sq._idx[current_idx] = neighbor._idx[neighbor_idx]
                        idx_aux[current_idx] = False
            new_elements = np.count_nonzero(idx_aux)
            sq._idx[idx_aux] = self._idx[last:last+new_elements]
            last += new_elements
        
    def _getDofNumber(self):
        ret = 0
        for counter, sq in np.ndenumerate(self._squares):
            aux = np.copy(sq._space._dof)
            for i in range(self._dim):
                idx = list(counter)
                idx[i] -= 1
                if idx[i] >= 0:
                    c = np.min([sq._linked[i, 0].getContinuity(i), sq.getContinuity(i)])
                    aux[i] -= (1+c)
            ret += np.prod(aux) * sq._space.var_n
        return ret
    
    def _SetUpMatrices(self):
        self.A[:] = 0
        self.F[:] = 0
        for sq in self._squares.flat:
            A, F = sq.IntegrateEquations(self._alpha)
            idx = sq._idx
            self.A[np.ix_(idx, idx)] += A
            self.F[idx] += F
            
    def _Solve(self):
        c, low = scipy.linalg.cho_factor(self.A)
        self._alpha = scipy.linalg.cho_solve((c, low), self.F)
        # self._alpha, it = scipy.sparse.linalg.cg(
        #                                         self.A,
        #                                         self.F,
        #                                         x0=self._alpha,
        #                                         tol=1e-8,
        #                                         maxiter=10000,
        #                                         )
        # if it == 0:
        #     print("CG solver was succesful!")
        # else:
        #     print("CG solver couldn't reach tolerance after iter: {}".format(it))
        #self._alpha = scipy.linalg.solve(self.A, self.F, assume_a='pos')
        #print("Condition number for matrix A is: {:e}".format(
        #    np.linalg.cond(self.A)))
        
    def _PlotSolution(self):
        
        if self._dim == 1:
            fig, ax = plt.subplots()
            for sq in self._squares.flat:
                X = sq.getPhysCoordMesh()
                Y = sq.getValue([0], 0, self._alpha)
                ax.scatter(X, Y, s=10, color='blue', marker='+', lw=1)
        if self._dim == 2:
            # fig = plt.figure()
            # ax= fig.add_subplot(111, projection='3d')
            # for sq in self._squares.flat:
            #     X = sq.getPhysCoordVector(0)
            #     Y = sq.getPhysCoordVector(1)
            #     X, Y = np.meshgrid(X, Y)
                
            #     Z = sq.getValue([0, 0], 0, self._alpha).reshape(X.shape[::-1]).T
            #     ax.plot_surface(X, Y, Z, color='blue')
            
            if self.ax is None:
                self.fig = plt.figure()
                self.ax= self.fig.add_subplot(111)
            self.ax.clear()
            for sq in self._squares.flat:
                X = sq.getPhysCoordVector(0)
                Y = sq.getPhysCoordVector(1)
                X, Y = np.meshgrid(X, Y)

                U = sq.getValue([0, 0], 0, self._alpha).reshape(X.shape[::-1]).T
                V = sq.getValue([0, 0], 1, self._alpha).reshape(X.shape[::-1]).T
                Unorm = U / np.sqrt(U**2 + V**2)
                Vnorm = V / np.sqrt(U**2 + V**2)
                self.ax.quiver(X, Y, Unorm, Vnorm, units = 'xy', width = 0.0008, scale = 75)
            #     X = sq.getPhysCoordVector(0)
            #     Y = sq.getPhysCoordVector(1)
            #     X, Y = np.meshgrid(X, Y)
            
            #     U = sq.getValue([0, 0], 0, self._alpha).reshape(X.shape[::-1]).T
            #     V = sq.getValue([0, 0], 1, self._alpha).reshape(X.shape[::-1]).T
            #     self.ax.quiver(X, Y, U, V, width=0.001, scale = 50
            # )
            self.fig.canvas.draw()
            self.fig.canvas.flush_events()
            return 

class StructuredMesh():
    def __init__(self, initial_vectors):
        self._coord_vectors = initial_vectors
        self._dim = len(self._coord_vectors)
        self._element_n = np.array([v.size - 1 for v in self._coord_vectors])
        self._meshgrid = np.array(np.meshgrid(
            *self._coord_vectors, indexing='ij'),
            dtype=float)
        self._points = np.empty_like(self._meshgrid[0], dtype = object)
        
        for n, counter in enumerate(product(*[range(len(x)) for x in self._coord_vectors])):
            print(n, counter)
            self._points[counter] = Point(self._meshgrid[(slice(None),) + counter], n)
        self.element_limits = [np.column_stack(
            [self._coord_vectors[i][0:-1], self._coord_vectors[i][1:]]
            ) for i in range(self._dim)]
        self._elements = [CartesianMeshElement(
                            n,
                           [self.element_limits[i][counter[-1-i], :] for i in range(len(counter))]
                           )
                         for n, counter in 
                         enumerate(product(*[range(x) for x in self._element_n]))]
        
    def setSolvingSpace(self, space):
        for e in self._elements:
            e.setSolvingSpace(space)
        # The initialization of the 'alpha' vector for the entire problem has
        # to be done here.
        
    def Plot(self):
        if self._dim == 1:
            #TODO annotate elements (print numbers in screen)
            fig, ax = plt.subplots()
            x = self._coord_vectors[0]
            y = np.zeros_like(x)
            ax.scatter(x, y, s=5)
            ax.add_collection(
                LineCollection(
                    (np.stack((x, y), axis=1),),
                    linewidths=0.5
                )
            )
        elif self._dim == 2:
            fig, ax = plt.subplots()
            x, y = np.meshgrid(*self._coord_vectors)
            segs_x = np.stack((x, y,), axis=2)
            segs_y = segs_x.transpose(1, 0, 2)
            ax.scatter(x, y, s=5)
            ax.add_collection(LineCollection(segs_x, linewidths=0.5))
            ax.add_collection(LineCollection(segs_y, linewidths=0.5))
            for e in self._elements:
                ax.annotate("{:d}".format(e.ID), [e.coords[0].mean(), e.coords[1].mean(0)],
                            ha="center", va="center", fontsize = 3)
        elif self._dim >= 3:
            fig = plt.figure()
            ax = Axes3D(fig)
            x, y, z = self._coord_vectors
            ax.grid(False)
            kwargs = {"alpha": 0.5, 'color' : 'royalblue', "linewidth": 0.5}
            # For X
            x_max = np.max(x)
            x_min = np.min(x)
            for in_y, in_z in product(y, z):
                ax.plot3D([x_min, x_max], [in_y, in_y], [in_z, in_z], **kwargs)
            y_max = np.max(y)
            y_min = np.min(y)
            for in_x, in_z in product(x, z):
                ax.plot3D([in_x, in_x], [y_min, y_max], [in_z, in_z], **kwargs)
            z_max = np.max(z)
            z_min = np.min(z)
            for in_x, in_y in product(x, y):
                ax.plot3D([in_x, in_x], [in_y, in_y], [z_min, z_max], **kwargs)
            ax.scatter(self._meshgrid[0].reshape(-1), 
                       self._meshgrid[1].reshape(-1),
                       self._meshgrid[2].reshape(-1), 
                       s = 5,
                       alpha = 1)
            for e in self._elements:
                ax.text(e.coords[0].mean(), e.coords[1].mean(0), e.coords[2].mean(), "{:d}".format(e.ID),
                            fontsize = 3)
            ax.set_xlabel("X")
            ax.set_ylabel("Y")
            ax.set_zlabel("Z")

def FindSquare(point):
    bottom_corner = point
    paths = np.array([x for x in permutations([i for i in range(point._dim)])])
    sense = np.ones_like(paths[0])
    connected = np.zeros(len(paths), dtype = bool)
    for n, p in enumerate(paths):
        if point.FindByPath(p, sense) is not None:
            connected[n] = True
        else:
            connected[0] = False
            break
    
    return np.all(connected)
    

if __name__ == "__main__":
    # points = np.array([Point(p, n) for n, p in enumerate(product(np.array(range(2)) * 0.5, repeat = 3))])
        
    # P1 = Point([0, 0, 0], 0)
    # P2 = Point([0, 1, 0], 1)
    # P3 = Point([1, 1, 0], 2)
    # P4 = Point([1, 1, 1], 3)
    # P4 = Point([1, 1], 3)
    # P1.ConnectTo(P2)
    # P2.ConnectTo(P3)
    # P3.ConnectTo(P4)
    # P1.ConnectTo(P3)
    # P1.ConnectTo(P4)
    # P2.ConnectTo(P4)
    # P3.ConnectTo(P4)
    # print((P1.FindByPath([0, 1, 0, 1], [1, 1, -1, -1])))
    # FindSquare(P1)
    def eq(e, config=None):
        X = e.getPhysCoordMesh()[:, 0]
        D = e.getFieldOperator([1])
        
        K = D
        G = np.pi * np.sin(np.pi * X) + np.pi
        e.setSystemOp(K, G)
        if e._linked[0, 0] is None:
            Bx0 = e.getBoundaryOperator(0, 0, [0])
            Gbc = np.ones(G.shape) - np.pi
            e.setBoundaryOp(Bx0, Gbc, 0)
        return e.A, e.F
    analytical_005 = lambda x, y: x/2 + y/4
    analytical_005x = lambda x, y: 1/2*np.ones_like(x)
    analytical_005y = lambda x, y: 1/4*np.ones_like(y)
    def eq2(e, config=None):
        X = e.getPhysCoordMesh()[:, 0]
        Y = e.getPhysCoordMesh()[:, 1]
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
        
        K = Dx + Dy
        G = analytical_005x(X, Y) + analytical_005y(X, Y)
        e.setSystemOp(K, G)
        
        if e._linked[1, 1] is None:  
            By0 = e.getBoundaryOperator(1, 1, [0, 0])
            Gbc = analytical_005(X, e.vecZ)
            BC = By0
            e.setBoundaryOp(BC, Gbc, 1)
        
        if e._linked[0, 1] is None:
            print("Go here")
            Bx1 = e.getBoundaryOperator(0, 1, [0, 0])
            BC = Bx1
            Gbc = analytical_005(np.ones_like(e.vecZ), Y)
            e.setBoundaryOp(BC, Gbc, 0)
            
        return e.A, e.F
    analytical_006 = lambda x, y: 2 * y * np.sin(4*np.pi*x)
    analytical_006x = lambda x, y: 8 * np.pi * y * np.cos(4*np.pi*x)
    analytical_006xx = lambda x, y: -32* y *  np.pi ** 2 * np.sin(4 * np.pi * x)
    analytical_006y = lambda x, y: 2 * np.sin(4*np.pi*x)
    analytical_006yy = lambda x, y: np.zeros_like(x)
    def eq3(e, config=None):
        X = e.getPhysCoordMesh()[:, 0]
        Y = e.getPhysCoordMesh()[:, 1]
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
    
    
        K1 = np.hstack([Dy, Dx])
        K2 = np.hstack([Dx, -H])
        K = np.vstack([K1, K2])
        G = np.hstack([analytical_006xx(X, Y) + analytical_006y(X, Y), e.vecZ])
        e.setSystemOp(K, G)
        BC2 = np.hstack([e.matZ] * 2)
        # y = 0
        if e._linked[1, 0] is None:
            By0 = e.getBoundaryOperator(1, 0, [0, 0])
            BC1 = np.hstack([By0] + [e.matZ])
            
            BC = np.vstack([BC1, BC2])
            Gbc = np.hstack([analytical_006(X, 0)]+[e.vecZ])
            e.setBoundaryOp(BC, Gbc, 1)
    
        # x = 1     
        if e._linked[0, 1] is None:
            Bx1 = e.getBoundaryOperator(0, 1, [0, 0])
            BC1 = np.hstack([Bx1] + [e.matZ])
            BC = np.vstack([BC1, BC2])
            Gbc = np.hstack([analytical_006(1, Y)]+[e.vecZ])
            e.setBoundaryOp(BC, Gbc, 0)
            
        # x = 0
        if e._linked[0, 0] is None:
            Bx0 = e.getBoundaryOperator(0, 0, [0, 0])
            BC1 = np.hstack([Bx0] + [e.matZ])
            BC = np.vstack([BC1, BC2])
            Gbc = np.hstack([analytical_006(-2, Y)] + [e.vecZ])
            e.setBoundaryOp(BC, Gbc, 0)
        
        # y = 1
        if e._linked[1, 1] is None:
            By1 = e.getBoundaryOperator(1, 1, [0, 1])
            BC1 = np.hstack([By1] + [e.matZ])
            BC = np.vstack([BC1, BC2])
            Gbc = np.hstack([analytical_006y(X, 1)] + [e.vecZ])
            e.setBoundaryOp(BC, Gbc, 1)
        
        if e.isInside([0, 0.5]):
            PO = e.getPointOperator([0, 0], [0, 0.5])
            PO = np.hstack([PO, e.matZ])
            PO = np.vstack([PO, BC2])
            e.setPointOp(PO, np.hstack([500*np.ones_like(e.vecZ)]*2))
        
        return e.A, e.F
    def f(x, center, width, beta):
        ret = np.zeros_like(x)
        ret[x>center] = np.tanh(beta*(-(x[x>center]-width/2-center)))
        ret[x<center] = np.tanh(beta*((x[x<center]+width/2-center)))
        return ret
    
    
    def LidCavity(e, alpha, config=None):
        e.A = np.zeros_like(e.A)
        e.F = np.zeros_like(e.F)
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
        X = e.getPhysCoordMesh()[:, 0]
        u = e.getValue([0, 0], 0, alpha)
        v = e.getValue([0, 0], 1, alpha)
        conv = u.reshape(-1, 1) * Dx + v.reshape(-1, 1) * Dy
        var_n = e._space.var_n
        K1 = np.hstack([conv, e.matZ, Dy/1000, Dx])
        K2 = np.hstack([e.matZ, conv, -Dx/1000, Dy])
        K3 = np.hstack([Dy, -Dx, H, e.matZ])
        K4 = np.hstack([Dx, Dy, e.matZ, e.matZ])
        K = np.vstack([K1, K2, K3, K4])
        G = np.hstack([e.vecZ]*var_n)
        e.setSystemOp(K, G)
        
        empty_row = np.hstack([e.matZ]*var_n)
        Gbc = np.hstack([e.vecZ]*var_n)
        # x = 0
        if e._linked[0, 0] is None:
            B = e.getBoundaryOperator(0, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC, Gbc, 0)
    
        # x = 1     
        if e._linked[0, 1] is None:
            B = e.getBoundaryOperator(0, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            e.setBoundaryOp(BC, Gbc, 0)
            
        # y = 0
        if e._linked[1, 0] is None:
            B = e.getBoundaryOperator(1, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC, Gbc, 1)
        
        # y = 1
        if e._linked[1, 1] is None:
            B = e.getBoundaryOperator(1, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            G1 = f(X, 0.5, 1, 50)
            G = np.hstack([G1]+[e.vecZ]*3)
            e.setBoundaryOp(BC, G, 1)
        
        if e.isInside([0, 0]):
            PO = e.getPointOperator([0, 0], [0, 0])
            PO = np.hstack([e.matZ]*3+[PO])
            PO = np.vstack([PO]+[empty_row]*3)
            e.setPointOp(PO, np.hstack([e.vecZ]*4))
        
        return e.A, e.F
    
    def LidCavity2(e, alpha, config=None):
        e.A = np.zeros_like(e.A)
        e.F = np.zeros_like(e.F)
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
        X = e.getPhysCoordMesh()[:, 0]
        u = e.getValue([0, 0], 0, alpha)
        v = e.getValue([0, 0], 1, alpha)
        conv = u.reshape(-1, 1) * Dx + v.reshape(-1, 1) * Dy
        var_n = e._space.var_n
        K1 = np.hstack([conv, e.matZ, Dy/3200, Dx])
        K2 = np.hstack([e.matZ, conv, -Dx/3200, Dy])
        K3 = np.hstack([Dy, -Dx, H, e.matZ])
        K4 = np.hstack([Dx, Dy, e.matZ, e.matZ])
        K = np.vstack([K1, K2, K3, K4])
        G = np.hstack([e.vecZ]*var_n)
        e.setSystemOp(K, G)
        
        empty_row = np.hstack([e.matZ]*var_n)
        Gbc = np.hstack([e.vecZ]*var_n)
        # x = 0
        if e._linked[0, 0] is None:
            B = e.getBoundaryOperator(0, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC, Gbc, 0)
    
        # x = 1     
        if e._linked[0, 1] is None:
            B = e.getBoundaryOperator(0, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            e.setBoundaryOp(BC, Gbc, 0)
            
        # y = 0
        if e._linked[1, 0] is None:
            B = e.getBoundaryOperator(1, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC, Gbc, 1)
        
        # y = 1
        if e._linked[1, 1] is None:
            B = e.getBoundaryOperator(1, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            G1 = f(X, 0.5, 1, 50)
            G = np.hstack([G1]+[e.vecZ]*3)
            e.setBoundaryOp(BC, G, 1)
        
        if e.isInside([0, 0]):
            PO = e.getPointOperator([0, 0], [0, 0])
            PO = np.hstack([e.matZ]*3+[PO])
            PO = np.vstack([PO]+[empty_row]*3)
            e.setPointOp(PO, np.hstack([e.vecZ]*4))
        
        return e.A, e.F
    
    def LidCavity22(e, alpha, config=None):
        e.A = np.zeros_like(e.A)
        e.F = np.zeros_like(e.F)
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
        X = e.getPhysCoordMesh()[:, 0]
        u = e.getValue([0, 0], 0, alpha).reshape(-1, 1)
        v = e.getValue([0, 0], 1, alpha).reshape(-1, 1)
        ux = e.getValue([1, 0], 0, alpha).reshape(-1, 1)
        uy = e.getValue([0, 1], 0, alpha).reshape(-1, 1)
        vx = e.getValue([1, 0], 1, alpha).reshape(-1, 1)
        vy = e.getValue([0, 1], 1, alpha).reshape(-1, 1)
        
        conv = u * Dx + v * Dy
        UU = conv + ux * H
        UV = uy * H
        VV = conv + vy * H
        VU = vx * H
        
        GU = u * ux + v * uy
        GV = u * vx + v * vy
        GU = np.squeeze(GU)
        GV = np.squeeze(GV)
        var_n = e._space.var_n
        K1 = np.hstack([UU, UV, Dy/3200, Dx])
        K2 = np.hstack([VU, VV, -Dx/3200, Dy])
        K3 = np.hstack([Dy, -Dx, H, e.matZ])
        K4 = np.hstack([Dx, Dy, e.matZ, e.matZ])
        K = np.vstack([K1, K2, K3, K4])
        G = np.hstack([GU, GV]+[e.vecZ]*2)
        e.setSystemOp(K, G)
        
        empty_row = np.hstack([e.matZ]*var_n)
        Gbc = np.hstack([e.vecZ]*var_n)
        WEIGHT = 100
        # x = 0
        if e._linked[0, 0] is None:
            B = e.getBoundaryOperator(0, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
    
        # x = 1     
        if e._linked[0, 1] is None:
            B = e.getBoundaryOperator(0, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
            
        # y = 0
        if e._linked[1, 0] is None:
            B = e.getBoundaryOperator(1, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 1)
        
        # y = 1
        if e._linked[1, 1] is None:
            B = e.getBoundaryOperator(1, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            G1 = f(X, 0.5, 1, 50)
            G = np.hstack([G1]+[e.vecZ]*3)
            e.setBoundaryOp(BC*WEIGHT, G*WEIGHT, 1)
        
        if e.isInside([0, 0]):
            PO = e.getPointOperator([0, 0], [0, 0])
            PO = np.hstack([e.matZ]*3+[PO])
            PO = np.vstack([PO]+[empty_row]*3)
            e.setPointOp(PO*WEIGHT, np.hstack([e.vecZ]*4))
        
        return e.A, e.F

    def LidCavity33(e, alpha, config=None):
        e.A = np.zeros_like(e.A)
        e.F = np.zeros_like(e.F)
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
        X = e.getPhysCoordMesh()[:, 0]
        u = e.getValue([0, 0], 0, alpha).reshape(-1, 1)
        v = e.getValue([0, 0], 1, alpha).reshape(-1, 1)
        ux = e.getValue([1, 0], 0, alpha).reshape(-1, 1)
        uy = e.getValue([0, 1], 0, alpha).reshape(-1, 1)
        vx = e.getValue([1, 0], 1, alpha).reshape(-1, 1)
        vy = e.getValue([0, 1], 1, alpha).reshape(-1, 1)
        
        conv = u * Dx + v * Dy
        UU = conv + ux * H
        UV = uy * H
        VV = conv + vy * H
        VU = vx * H
        
        GU = u * ux + v * uy
        GV = u * vx + v * vy
        GU = np.squeeze(GU)
        GV = np.squeeze(GV)
        var_n = e._space.var_n
        K1 = np.hstack([UU, UV, Dy/1000, Dx])
        K2 = np.hstack([VU, VV, -Dx/1000, Dy])
        K3 = np.hstack([Dy, -Dx, H, e.matZ])
        K4 = np.hstack([Dx, Dy, e.matZ, e.matZ])
        K = np.vstack([K1, K2, K3, K4])
        G = np.hstack([GU, GV]+[e.vecZ]*2)
        e.setSystemOp(K, G)
        
        empty_row = np.hstack([e.matZ]*var_n)
        Gbc = np.hstack([e.vecZ]*var_n)
        WEIGHT = 100
        # x = 0
        if e._linked[0, 0] is None:
            B = e.getBoundaryOperator(0, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
    
        # x = 1     
        if e._linked[0, 1] is None:
            B = e.getBoundaryOperator(0, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
            
        # y = 0
        if e._linked[1, 0] is None:
            B = e.getBoundaryOperator(1, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 1)
        
        # y = 1
        if e._linked[1, 1] is None:
            B = e.getBoundaryOperator(1, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            G1 = f(X, 0.5, 1, 50)
            G = np.hstack([G1]+[e.vecZ]*3)
            e.setBoundaryOp(BC*WEIGHT, G*WEIGHT, 1)
        
        if e.isInside([0, 0]):
            PO = e.getPointOperator([0, 0], [0, 0])
            PO = np.hstack([e.matZ]*3+[PO])
            PO = np.vstack([PO]+[empty_row]*3)
            e.setPointOp(PO*WEIGHT, np.hstack([e.vecZ]*4))
        
        return e.A, e.F
    
    def LidCavity4(e, alpha, config=None):
        e.A = np.zeros_like(e.A)
        e.F = np.zeros_like(e.F)
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
        X = e.getPhysCoordMesh()[:, 0]
        u = e.getValue([0, 0], 0, alpha).reshape(-1, 1)
        v = e.getValue([0, 0], 1, alpha).reshape(-1, 1)
        ux = e.getValue([1, 0], 0, alpha).reshape(-1, 1)
        uy = e.getValue([0, 1], 0, alpha).reshape(-1, 1)
        vx = e.getValue([1, 0], 1, alpha).reshape(-1, 1)
        vy = e.getValue([0, 1], 1, alpha).reshape(-1, 1)
        
        conv = u * Dx + v * Dy
        UU = conv + ux * H
        UV = uy * H
        VV = conv + vy * H
        VU = vx * H
        
        GU = u * ux + v * uy
        GV = u * vx + v * vy
        GU = np.squeeze(GU)
        GV = np.squeeze(GV)
        var_n = e._space.var_n
        K1 = np.hstack([UU, UV, Dy/2000, Dx])
        K2 = np.hstack([VU, VV, -Dx/2000, Dy])
        K3 = np.hstack([Dy, -Dx, H, e.matZ])
        K4 = np.hstack([Dx, Dy, e.matZ, e.matZ])
        K = np.vstack([K1, K2, K3, K4])
        G = np.hstack([GU, GV]+[e.vecZ]*2)
        e.setSystemOp(K, G)
        
        empty_row = np.hstack([e.matZ]*var_n)
        Gbc = np.hstack([e.vecZ]*var_n)
        WEIGHT = 100
        # x = 0
        if e._linked[0, 0] is None:
            B = e.getBoundaryOperator(0, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
    
        # x = 1     
        if e._linked[0, 1] is None:
            B = e.getBoundaryOperator(0, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
            
        # y = 0
        if e._linked[1, 0] is None:
            B = e.getBoundaryOperator(1, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 1)
        
        # y = 1
        if e._linked[1, 1] is None:
            B = e.getBoundaryOperator(1, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            G1 = f(X, 0.5, 1, 50)
            G = np.hstack([G1]+[e.vecZ]*3)
            e.setBoundaryOp(BC*WEIGHT, G*WEIGHT, 1)
        
        if e.isInside([0, 0]):
            PO = e.getPointOperator([0, 0], [0, 0])
            PO = np.hstack([e.matZ]*3+[PO])
            PO = np.vstack([PO]+[empty_row]*3)
            e.setPointOp(PO*WEIGHT, np.hstack([e.vecZ]*4))
        
        return e.A, e.F
    
    def LidCavity5(e, alpha, config=None):
        e.A = np.zeros_like(e.A)
        e.F = np.zeros_like(e.F)
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
        X = e.getPhysCoordMesh()[:, 0]
        u = e.getValue([0, 0], 0, alpha).reshape(-1, 1)
        v = e.getValue([0, 0], 1, alpha).reshape(-1, 1)
        ux = e.getValue([1, 0], 0, alpha).reshape(-1, 1)
        uy = e.getValue([0, 1], 0, alpha).reshape(-1, 1)
        vx = e.getValue([1, 0], 1, alpha).reshape(-1, 1)
        vy = e.getValue([0, 1], 1, alpha).reshape(-1, 1)
        
        conv = u * Dx + v * Dy
        UU = conv + ux * H
        UV = uy * H
        VV = conv + vy * H
        VU = vx * H
        
        GU = u * ux + v * uy
        GV = u * vx + v * vy
        GU = np.squeeze(GU)
        GV = np.squeeze(GV)
        var_n = e._space.var_n
        K1 = np.hstack([UU, UV, Dy/3000, Dx])
        K2 = np.hstack([VU, VV, -Dx/3000, Dy])
        K3 = np.hstack([Dy, -Dx, H, e.matZ])
        K4 = np.hstack([Dx, Dy, e.matZ, e.matZ])
        K = np.vstack([K1, K2, K3, K4])
        G = np.hstack([GU, GV]+[e.vecZ]*2)
        e.setSystemOp(K, G)
        
        empty_row = np.hstack([e.matZ]*var_n)
        Gbc = np.hstack([e.vecZ]*var_n)
        WEIGHT = 100
        # x = 0
        if e._linked[0, 0] is None:
            B = e.getBoundaryOperator(0, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
    
        # x = 1     
        if e._linked[0, 1] is None:
            B = e.getBoundaryOperator(0, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
            
        # y = 0
        if e._linked[1, 0] is None:
            B = e.getBoundaryOperator(1, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 1)
        
        # y = 1
        if e._linked[1, 1] is None:
            B = e.getBoundaryOperator(1, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            G1 = f(X, 0.5, 1, 50)
            G = np.hstack([G1]+[e.vecZ]*3)
            e.setBoundaryOp(BC*WEIGHT, G*WEIGHT, 1)
        
        if e.isInside([0, 0]):
            PO = e.getPointOperator([0, 0], [0, 0])
            PO = np.hstack([e.matZ]*3+[PO])
            PO = np.vstack([PO]+[empty_row]*3)
            e.setPointOp(PO*WEIGHT, np.hstack([e.vecZ]*4))
        
        return e.A, e.F
    
    def LidCavity6(e, alpha, config=None):
        e.A = np.zeros_like(e.A)
        e.F = np.zeros_like(e.F)
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
        X = e.getPhysCoordMesh()[:, 0]
        u = e.getValue([0, 0], 0, alpha).reshape(-1, 1)
        v = e.getValue([0, 0], 1, alpha).reshape(-1, 1)
        ux = e.getValue([1, 0], 0, alpha).reshape(-1, 1)
        uy = e.getValue([0, 1], 0, alpha).reshape(-1, 1)
        vx = e.getValue([1, 0], 1, alpha).reshape(-1, 1)
        vy = e.getValue([0, 1], 1, alpha).reshape(-1, 1)
        
        conv = u * Dx + v * Dy
        UU = conv + ux * H
        UV = uy * H
        VV = conv + vy * H
        VU = vx * H
        
        GU = u * ux + v * uy
        GV = u * vx + v * vy
        GU = np.squeeze(GU)
        GV = np.squeeze(GV)
        var_n = e._space.var_n
        K1 = np.hstack([UU, UV, Dy/4000, Dx])
        K2 = np.hstack([VU, VV, -Dx/4000, Dy])
        K3 = np.hstack([Dy, -Dx, H, e.matZ])
        K4 = np.hstack([Dx, Dy, e.matZ, e.matZ])
        K = np.vstack([K1, K2, K3, K4])
        G = np.hstack([GU, GV]+[e.vecZ]*2)
        e.setSystemOp(K, G)
        
        empty_row = np.hstack([e.matZ]*var_n)
        Gbc = np.hstack([e.vecZ]*var_n)
        WEIGHT = 100
        # x = 0
        if e._linked[0, 0] is None:
            B = e.getBoundaryOperator(0, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
    
        # x = 1     
        if e._linked[0, 1] is None:
            B = e.getBoundaryOperator(0, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
            
        # y = 0
        if e._linked[1, 0] is None:
            B = e.getBoundaryOperator(1, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 1)
        
        # y = 1
        if e._linked[1, 1] is None:
            B = e.getBoundaryOperator(1, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            G1 = f(X, 0.5, 1, 50)
            G = np.hstack([G1]+[e.vecZ]*3)
            e.setBoundaryOp(BC*WEIGHT, G*WEIGHT, 1)
        
        if e.isInside([0, 0]):
            PO = e.getPointOperator([0, 0], [0, 0])
            PO = np.hstack([e.matZ]*3+[PO])
            PO = np.vstack([PO]+[empty_row]*3)
            e.setPointOp(PO*WEIGHT, np.hstack([e.vecZ]*4))
        
        return e.A, e.F
    
    def LidCavity7(e, alpha, config=None):
        e.A = np.zeros_like(e.A)
        e.F = np.zeros_like(e.F)
        Dx = e.getFieldOperator([1, 0])
        Dy = e.getFieldOperator([0, 1])
        H = e.getFieldOperator([0, 0])
        X = e.getPhysCoordMesh()[:, 0]
        u = e.getValue([0, 0], 0, alpha).reshape(-1, 1)
        v = e.getValue([0, 0], 1, alpha).reshape(-1, 1)
        ux = e.getValue([1, 0], 0, alpha).reshape(-1, 1)
        uy = e.getValue([0, 1], 0, alpha).reshape(-1, 1)
        vx = e.getValue([1, 0], 1, alpha).reshape(-1, 1)
        vy = e.getValue([0, 1], 1, alpha).reshape(-1, 1)
        
        conv = u * Dx + v * Dy
        UU = conv + ux * H
        UV = uy * H
        VV = conv + vy * H
        VU = vx * H
        
        GU = u * ux + v * uy
        GV = u * vx + v * vy
        GU = np.squeeze(GU)
        GV = np.squeeze(GV)
        var_n = e._space.var_n
        K1 = np.hstack([UU, UV, Dy/5000, Dx])
        K2 = np.hstack([VU, VV, -Dx/5000, Dy])
        K3 = np.hstack([Dy, -Dx, H, e.matZ])
        K4 = np.hstack([Dx, Dy, e.matZ, e.matZ])
        K = np.vstack([K1, K2, K3, K4])
        G = np.hstack([GU, GV]+[e.vecZ]*2)
        e.setSystemOp(K, G)
        
        empty_row = np.hstack([e.matZ]*var_n)
        Gbc = np.hstack([e.vecZ]*var_n)
        WEIGHT = 100
        # x = 0
        if e._linked[0, 0] is None:
            B = e.getBoundaryOperator(0, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
    
        # x = 1     
        if e._linked[0, 1] is None:
            B = e.getBoundaryOperator(0, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 0)
            
        # y = 0
        if e._linked[1, 0] is None:
            B = e.getBoundaryOperator(1, 0, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            
            e.setBoundaryOp(BC*WEIGHT, Gbc*WEIGHT, 1)
        
        # y = 1
        if e._linked[1, 1] is None:
            B = e.getBoundaryOperator(1, 1, [0, 0])
            BC1 = np.hstack([B] + [e.matZ] * (var_n-1))
            BC2 = np.hstack([e.matZ]+[B]+[e.matZ]*(var_n-2))
            BC = np.vstack([BC1, BC2]+[empty_row]*2)
            G1 = f(X, 0.5, 1, 50)
            G = np.hstack([G1]+[e.vecZ]*3)
            e.setBoundaryOp(BC*WEIGHT, G*WEIGHT, 1)
        
        if e.isInside([0, 0]):
            PO = e.getPointOperator([0, 0], [0, 0])
            PO = np.hstack([e.matZ]*3+[PO])
            PO = np.vstack([PO]+[empty_row]*3)
            e.setPointOp(PO*WEIGHT, np.hstack([e.vecZ]*4))
        
        return e.A, e.F

    mve = MultiVariableElement(2, 4)
    mve.setBasisFunction(BasisFunction.Hermite)
    mve.setDof([6,6])
    mve.setQuadPoints([8, 8])
    x = GLLSpace([0, 1], 18)
    y = GLLSpace([0, 1], 18)
    z = np.linspace(-2, 0, 6)
    m1 = GlobalFiniteElement((x,y))
    m1.setSolvingSpaceForAll(mve)
    m1.setContinuityForAll([1, 1])
    res = 0.0001
    # p = m1._points[1]
    # print(FindSquare(p))
    # m1.Plot()
    # print(m1._getDofNumber())
    # t1 = time()
    m1._AssignDof()
    # t2 = time()
    m1.setEquationForAll(LidCavity)
    rel = 1
    while rel > 1e-9:
        print("Solving for {:}".format(str(m1._squares.flat[0]._equation.__name__)))
        t1 = time()
        m1._SetUpMatrices()
        # t3 = time()
        t2 = time()
        print("Solving for {:} dofs".format(m1._dof_n))
        m1._Solve()
        
        new = 0
        t3 = time()
        for sq in m1._squares.flat:
            new += sq.getResidual(m1._alpha)
        new = new ** 0.5
        print("New residual: {:e}".format(new))
        rel = np.abs(new - res)/res
        print("New relative residual: {:e}".format(rel))
        print("Total time taken: {:}".format(t3-t1))
        total_t = t3 - t1
        print("Time setting matrices: {:.2%}".format((t2 - t1)/total_t))
        print("Time solving the equation: {:.2%}".format((t3 - t2)/total_t))
        
        res = new
        m1._PlotSolution()
        plt.pause(0.05)
    m1.setEquationForAll(LidCavity2)
    rel = 1
    while rel > 1e-9:
        print("Solving for {:}".format(str(m1._squares.flat[0]._equation.__name__)))
        t1 = time()
        m1._SetUpMatrices()
        # t3 = time()
        t2 = time()
        print("Solving for {:} dofs".format(m1._dof_n))
        m1._Solve()
        
        new = 0
        t3 = time()
        for sq in m1._squares.flat:
            new += sq.getResidual(m1._alpha)
        new = new ** 0.5
        print("New residual: {:e}".format(new))
        rel = np.abs(new - res)/res
        print("New relative residual: {:e}".format(rel))
        print("Total time taken: {:}".format(t3-t1))
        total_t = t3 - t1
        print("Time setting matrices: {:.2%}".format((t2 - t1)/total_t))
        print("Time solving the equation: {:.2%}".format((t3 - t2)/total_t))
        
        res = new
        m1._PlotSolution()
        plt.pause(0.05)
    # t4 = time()
    # m1._PlotSolution()
    # m1.setEquationForAll(LidCavity2)
    # while rel > 1e-9:
    #     print("Solving for {:}".format(str(m1._squares.flat[0]._equation.__name__)))
    #     t1 = time()
    #     m1._SetUpMatrices()
    #     # t3 = time()
    #     t2 = time()
    #     print("Solving for {:} dofs".format(m1._dof_n))
    #     m1._Solve()
        
    #     new = 0
    #     t3 = time()
    #     for sq in m1._squares.flat:
    #         new += sq.getResidual(m1._alpha)
    #     new = new ** 0.5
    #     print("New residual: {:e}".format(new))
    #     rel = np.abs(new - res)/res
    #     print("New relative residual: {:e}".format(rel))
    #     print("Total time taken: {:}".format(t3-t1))
    #     total_t = t3 - t1
    #     print("Time setting matrices: {:.2%}".format((t2 - t1)/total_t))
    #     print("Time solving the equation: {:.2%}".format((t3 - t2)/total_t))
        
    #     res = new

    # total_t = t4 - t1
    # print("Total time taken: {:}".format(total_t))
    # print("Time building mesh: {:.2%}".format((t2 - t1)/total_t))
    # print("Time setting matrices: {:.2%}".format((t3 - t2)/total_t))
    # print("Time solving the equation: {:.2%}".format((t4 - t3)/total_t))