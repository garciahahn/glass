#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 01:54:49 2021

@author: garciahahn
"""
from element import *
from highorder import *
from highorder import BasisFunction
import matplotlib.pyplot as plt
import numpy as np
import itertools as it
from scipy.sparse.linalg import cg
from scipy.sparse import csr_matrix


class Boundary:
    def __init__(self):
        self.K = None
        self.G = None
        self.set = False

    def Set(self, K, G):
        self.set = True
        self.K = K
        self.G = G


class MultiVariableElement:
    def __init__(self, dim, var_n):
        self._alpha = None
        self._basis_function = None
        self._quad_vector = None
        self._weight_vector = None
        self._quad_coords_mesh = None
        self._quad_coords_vector = None
        self._dof = None
        self._quad_n = None
        self._dof_n = None
        self.K = None
        self.G = None
        self.K_bc = None
        self.G_bc = None
        self.A = None
        self.F = None
        self.boundary_set = None
        if dim > 0 and isinstance(dim, int):
            self._dim = dim
            self.boundary_set = np.array(
                [[Boundary() for _ in range(2)] for _ in range(dim)])
        else:
            print("dim are of incorrect type or value.")
        if var_n > 0 and isinstance(var_n, int):
            self.var_n = var_n
        else:
            print("var_n are of incorrect type or value.")

    def setBasisFunction(self, basis_function):
        if isinstance(basis_function, BasisFunction):
            self._basis_function = basis_function
        else:
            print("basis_function is of the wrong type")

    def setDof(self, dof):
        dof = np.array(dof)
        if self._basis_function == BasisFunction.Lagrange and dof.size == self._dim and np.all(dof >= 2):
            self._dof = dof
            self._dof_n = np.prod(self._dof) * self.var_n
        elif self._basis_function == BasisFunction.Hermite and dof.size == self._dim and np.all(dof >= 4):
            self._dof = dof
            self._dof_n = np.prod(self._dof) * self.var_n
        else:
            print("dof are of incorrect type or value.")
        if self._dof is not None:
            self._dof_per_var = self._dof_n // self.var_n


    def setQuadPoints(self, quadrature_points):

        quadrature_points = np.array(quadrature_points)

        if self._basis_function == BasisFunction.Lagrange:
            self._quad = self._dof
        elif self._basis_function == BasisFunction.Hermite \
                and quadrature_points.size == self._dim \
                and np.all(self._dof <= quadrature_points):
            self._quad = quadrature_points
        else:
            print("oh oh, something is wrong")
            return

        self._quad_coords_vector = np.array(
            [GLLSpace(LIMITS, x) for x in self._quad], dtype=object)
        self._weight_vector = np.array(
            [GLLWeights(LIMITS, x) for x in self._quad], dtype=object)

# The generation of the mesh list is a little bit cumbersome but not that hard.
# It needs a couple of steps.

        # First we generate a tuple where we shift the index order of the
        # standard generated numpy meshgrid grid.
        re_order = tuple(x for x in range(1, self._dim+1)) + (0,)
        
        # We generate the mesh with the numpy meshgrid routine and it is stored
        # in a list by default.
        self._quad_coords_mesh = np.meshgrid(
                                            *self._quad_coords_vector,
                                            indexing='ij',
                                            )
        # Then we accomodate the list into a numpy array for further
        # manipulation.
        self._quad_coords_mesh = np.array(self._quad_coords_mesh, dtype=float)
        
        # Transpose the mesh to obtain the desired order of axis.
        self._quad_coords_mesh = self._quad_coords_mesh.transpose(re_order)
        # Reshape the mesh to have the values in a list formatted way.
        self._quad_coords_mesh = self._quad_coords_mesh.reshape(-1, self._dim)

# This kind of vector allows us to obtain the correct coordinate for a given #
# quadrature point. For example, the rows of a mass matrix H will correspond #
# to the coordinate given by the same row in 'self._quad_coords_mesh'        #
# Thus, when using 'X = element.getPhysCoordMesh()[:, 0]' with a             #
# CartesianMeshElement class will give us the vector corresponding to the X  #
# coordinates in the same order than those found in the mass matrix or       #
# derivative operators.                                                      #
#                                                                   Julián   #
        
        self._quad_n = np.prod(self._quad) * self.var_n
        self.vecZ = np.zeros(np.prod(self._quad))
        self.matZ = np.zeros([np.prod(self._quad), np.prod(self._dof)])

    def _getBasisFunction(self):
        if self._basis_function == BasisFunction.Lagrange:
            return
        elif self._basis_function == BasisFunction.Hermite:
            pass

    def getWeightVector(self, dim, jacobian = 1):
        return np.array(self._weight_vector[dim], np.float) * jacobian

    def getWeightMatrix(self, jacobian = None):
        if jacobian is None:
            jacobian = np.ones(self._dim)
        W = 1
        for counter in range(self._dim):
            W = np.kron(W, np.diag(self.getWeightVector(counter, jacobian[counter])))
        return np.kron(np.eye(self.var_n), W)

    def getWeightMatrixForDir(self, direction, jacobian = None):
        if jacobian is None:
            jacobian = np.ones(self._dim)
        W = 1
        for counter in range(self._dim):
            if counter == direction:
                W = np.kron(W, np.eye(self._quad[counter]))
            else:
                W = np.kron(W, np.diag(self.getWeightVector(counter, jacobian[counter])))
        return np.kron(np.eye(self.var_n), W)


    def getCoordsVector(self, dim, coord = None):
        if coord is not None:
            inf = np.min(coord)
            sup = np.max(coord)
            return (np.array(self._quad_coords_vector[dim], np.float) + 1) \
                / 2 * (sup - inf) + inf
        else:
            return np.array(self._quad_coords_vector[dim], np.float)

    def getFieldOperator(self, derivative, jacobian = None):
        if jacobian is None:
            jacobian = np.ones(self._dim)
        D = 1
        if self._basis_function == BasisFunction.Lagrange:
            for counter in range(self._dim):
                if derivative[counter] == 0:
                    x = self.getCoordsVector(counter)
                    aux = np.array([LagrangePolynomialGLLBased(
                    i, x, x.size) for i in range(x.size)])
                    aux = np.round(aux, 12)
                    aux[aux == .0] = .0
                elif derivative[counter] == 1:
                    aux = LagrangeDerivativePolynomialGLLBased(self._dof[counter]) / jacobian[counter]
                else:
                    print("Can't ask for a derivative greater than 1 for Lagrangian basis")
                    return
                D = np.kron(D, aux)
        elif self._basis_function == BasisFunction.Hermite:
            for counter in range(self._dim):
                x = self.getCoordsVector(counter)
                if derivative[counter] <= 2 and derivative[counter] >= 0:
                    H12 = np.array([Hermite(x, i, derivative[counter]) for i in range(2)]).T * np.array([1, jacobian[counter]])
                    H34 = np.array([Hermite(x, i, derivative[counter]) for i in range(2, 4)]).T * np.array([1, jacobian[counter]])
                    H_extra = np.array([Hermite(x, i, derivative[counter]) for i in range(4, self._dof[counter])]).T
                    if H_extra.size > 0:
                        H = np.concatenate([H12, H_extra, H34], axis=1)
                    else:
                        H = np.concatenate([H12, H34], axis=1)
                    H = np.round(H, 12)
                    H[H == .0] = .0
                    H /= (jacobian[counter] ** derivative[counter])
                else:
                    print("Cannot choose a higher derivative than 2 for Hermite")
                D = np.kron(D, H)
        return D

    def getBoundaryIndex(self, direction, boundary):
        value = -1 if boundary == 0 else 1
        if direction < self._dim:
            return np.where(np.abs(self._quad_coords_mesh[:, direction] - value) < TOL)
        else:
            print("Problems in getBoundaryIndex")
            return

    def getBoundaryOperator(self, direction, boundary, derivative, jacobian = None):
        if jacobian is None:
            jacobian = np.ones(self._dim)
        if direction < self._dim \
                and len(derivative) == self._dim:
            B = np.zeros([np.prod(self._quad), np.prod(self._dof)])
            idx = self.getBoundaryIndex(direction, boundary)
            B[idx, :] = self.getFieldOperator(derivative, jacobian)[idx, :]
            return B
        else:
            print("Whoopsie, problems with the boundary dimensions")
            return

    def getPointOperator(self, derivative, point, jacobian=None):
        if jacobian is None:
            jacobian = np.ones(self._dim)
        if len(point) != self._dim:
            raise ValueError("point should be of the same dimensions as"
                             " the problem. Problem is of dimension {:d} and "
                             "point has dimension {:d}.".format(
                                                         self._dim,
                                                         len(point),
                                                         ))
        idx = np.argmin(
                np.linalg.norm(
                    self._quad_coords_mesh - point, axis=1
                    )
                )
        
        OP = np.zeros([np.prod(self._quad), np.prod(self._dof)])
        field_OP = self.getFieldOperator(derivative, jacobian)
        OP[idx, :] = field_OP[idx, :]
        
        return OP

    def getValue(self, derivative, variable):
        H = self.getFieldOperator(derivative)
        alpha = self._alpha[variable * self._dof_per_var:
                            variable * self._dof_per_var + self._dof_per_var]
        return (H @ alpha)

    def setSystemOp(self, K, G):
        """Sets the system differential equations.
        

        Parameters
        ----------
        K : TYPE
            DESCRIPTION.
        G : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.K = K
        self.G = G
        W = self.getWeightMatrix()
        self.A += K.T @ W @ K
        self.F += K.T @ W @ G

    def setBoundaryOp(self, K, G, direction):
        W = self.getWeightMatrixForDir(direction)
        self.A += K.T @ W @ K
        self.F += K.T @ W @ G

    def chrrruck(self):
        self.solveSystem()

    def solveSystem(self):
        W = self.getWeightMatrix()
        # A = self.K.T @ W @ self.K
        # F = self.K.T @ W @ self.G
        # for (d, b) in it.product(range(self._dim), range(2)):
        #     if self.boundary_set[d, b].set:
        #         W = self.getWeightMatrixForDir(d)
        #         K = self.boundary_set[d, b].K
        #         A += K.T @ W @ K
        #         F += K.T @ W @ self.boundary_set[d, b].G

        self._alpha, _ = cg(csr_matrix(self.A), self.F)
        print("Condition number for matrix A is: {:e}".format(
            np.linalg.cond(np.linalg.inv(self.A))))
        R = self.K @ self._alpha - self.G
        self._residual = np.sqrt(R.T @ W @ R)

    def Plot2D(self, derivative, value):
        fig, ax = plt.subplots()
        X, Y = np.meshgrid(self.getCoordsVector(0), self.getCoordsVector(1))
        Z = self.getValue(derivative, value)
        #cm = ax.pcolormesh(X, Y, Z.reshape(X.shape))
        # plt.colorbar(cm)
        ax.quiver(self.getValue([0, 0], 0).reshape(
            X.shape), self.getValue([0, 0], 1).reshape(X.shape))
        #ax.imshow(X, Y, Z.reshape(X.shape))
        plt.show()


def test_2D():
    e = MultiVariableElement(2, 1)
    e.setBasisFunction(BasisFunction.Hermite)
    e.setDof([5, 5])
    e.setQuadPoints([15, 15])
    H = e.getFieldOperator([0, 0])
    Dx = e.getFieldOperator([1, 0])
    Dxx = e.getFieldOperator([2, 0])
    Dy = e.getFieldOperator([0, 1])
    Dyy = e.getFieldOperator([0, 2])

    B_valy1 = e.getBoundaryOperator(1, 0, [0, 0])
    B_valx0 = e.getBoundaryOperator(0, 0, [0, 0])

    K = Dx + Dy
    G = np.ones([np.prod(e._quad), 1]) * 3/4

    Gbc1 = np.zeros(G.shape)
    Gbc2 = np.zeros(G.shape)
    Kbc1 = B_valy1
    Kbc2 = B_valx0
    indx1 = e.getBoundaryIndex(1, 0)
    indx2 = e.getBoundaryIndex(0, 0)
    Gbc1[indx1] = e._quad_coords_mesh[indx1, 0].reshape([-1, 1])/2 - 1/4
    Gbc2[indx2] = e._quad_coords_mesh[indx2, 1].reshape([-1, 1])/4 - 1/2

    e.setSystemOp(K, G)
    e.setBoundaryOp(Kbc1, Gbc1)
    e.setBoundaryOp(Kbc2, Gbc2)
    e.solveSystem()

    X, Y = np.meshgrid(e.getCoordsVector(0), e.getCoordsVector(1))
    Z = e.getValue([0, 0], 0)
    #Z = H @ alpha
    # Z_analytical = X/2 + Y/4

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.plot_surface(X, Y, Z.reshape(X.shape))
    plt.show()


if __name__ == "__main__":
    e = MultiVariableElement(4, 1)
    e.setBasisFunction(BasisFunction.Hermite)
    e.setDof([4]*4)
    e.setQuadPoints([x for x in range (4, 8)])
    # H = e.getFieldOperator([0, 0])
    # Dx = e.getFieldOperator([1, 0])
    # Dxx = e.getFieldOperator([2, 0])
    # Dy = e.getFieldOperator([0, 1])
    # Dyy = e.getFieldOperator([0, 2])
    # tol = 1
    # old_res = 1
    # matZ = np.zeros(H.shape)
    # vecZ = np.zeros(np.prod(e._quad))

    # G = np.concatenate([vecZ, vecZ, vecZ, vecZ], axis=0)

    # Bx0 = e.getBoundaryOperator(0, 0, [0, 0])
    # # x = 0
    # BC1 = np.concatenate([Bx0, matZ, matZ, matZ], axis=1)
    # BC2 = np.concatenate([matZ, Bx0, matZ, matZ], axis=1)
    # opZ = np.concatenate([matZ, matZ, matZ, matZ], axis=1)
    # BC1 = np.concatenate([BC1, BC2, opZ, opZ], axis=0)
    # Gx0 = np.zeros(G.shape)
    # indx0 = e.getBoundaryIndex(0, 0)
    # e.setBoundaryOp(BC1, Gx0, 0, 0)

    # # x = 1
    # Bx1 = e.getBoundaryOperator(0, 1, [0, 0])
    # BC1 = np.concatenate([Bx1, matZ, matZ, matZ], axis=1)
    # BC2 = np.concatenate([matZ, Bx1, matZ, matZ], axis=1)
    # opZ = np.concatenate([matZ, matZ, matZ, matZ], axis=1)
    # BC1 = np.concatenate([BC1, BC2, opZ, opZ], axis=0)
    # Gx1 = np.zeros(G.shape)
    # indx1 = e.getBoundaryIndex(0, 1)
    # e.setBoundaryOp(BC1, Gx1, 0, 1)

    # # y = 0
    # By0 = e.getBoundaryOperator(1, 0, [0, 0])
    # BC1 = np.concatenate([By0, matZ, matZ, matZ], axis=1)
    # BC2 = np.concatenate([matZ, By0, matZ, matZ], axis=1)
    # opZ = np.concatenate([matZ, matZ, matZ, matZ], axis=1)
    # BC1 = np.concatenate([BC1, BC2, opZ, opZ], axis=0)
    # Gy0 = np.zeros(G.shape)
    # indy0 = e.getBoundaryIndex(1, 0)
    # e.setBoundaryOp(BC1, Gy0, 1, 0)

    # # y = 1
    # By1 = e.getBoundaryOperator(1, 1, [0, 0])
    # BC1 = np.concatenate([By1, matZ, matZ, matZ], axis=1)
    # BC2 = np.concatenate([matZ, By1, matZ, matZ], axis=1)
    # opZ = np.concatenate([matZ, matZ, matZ, matZ], axis=1)
    # BC1 = np.concatenate([BC1, BC2, opZ, opZ], axis=0)
    # Gy1 = np.zeros(G.shape)
    # indy1 = e.getBoundaryIndex(1, 1)
    # Gy1[indy1] = np.ones(Gy1[indy1].shape)
    # e.setBoundaryOp(BC1, Gy1, 1, 1)

    # while tol > 1e-10:
    #     u = e.getValue([0, 0], 0)
    #     v = e.getValue([0, 0], 1)
    #     conv = u * Dx + v * Dy
    #     K1 = np.concatenate([conv, matZ, -(1/10)*Dy, Dx], axis=1)
    #     K2 = np.concatenate([matZ, conv, Dx, Dy], axis=1)
    #     K3 = np.concatenate([Dy, -Dx, H, matZ], axis=1)
    #     K4 = np.concatenate([Dx, Dy, matZ, matZ], axis=1)

    #     K = np.concatenate([K1, K2, K3, K4], axis=0)
    #     e.setSystemOp(K, G)
    #     e.solveSystem()
    #     new_res = e._residual
    #     tol = np.abs(new_res - old_res)/new_res
    #     old_res = new_res

    #     print(tol)
