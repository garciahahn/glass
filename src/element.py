#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 21 17:42:26 2021

@author: garciahahn
"""

from highorder import *
import matplotlib.pyplot as plt

LIMITS = (-1, 1)
TOL = 1e-9

class Element():
    def __init__(self, dim):
        self._dofs_vector = None
        self._quad_vector = None
        self._weight_vector = None
        self._quad_coords_mesh = None
        self._quad_coords_vector = None
        self._dofs = None
        self._quad_n = None
        self._dof_n = None
        if dim > 0 and isinstance(dim, int):
            self._dim = dim
        else:
            print("dim are of incorrect type or value.")
    def setBasisFunction(self, basis_function):
        if isinstance(basis_function, BasisFunction):
            self._basis_function = basis_function
        else:
            print("basis_function is of the wrong type")
            
    def setDof(self, dofs):
        dofs = np.array(dofs)
        if self._basis_function == BasisFunction.Lagrange and dofs.size == self._dim and np.all(dofs >= 2):
            self._dofs = dofs
            self._quad = dofs
            self._quad_coords_vector = np.array([GLLSpace(LIMITS, x) for x in self._dofs], dtype = object)
            self._weight_vector = np.array([GLLWeights(LIMITS, x) for x in self._dofs], dtype = object)
            self._quad_coords_mesh = np.array(np.meshgrid(*self._quad_coords_vector, indexing='ij'), dtype=float).T.reshape(-1, self._dim)
            self._quad_n = np.prod(self._quad)
            self._dof_n = np.prod(self._dofs)
        elif self._basis_function == BasisFunction.Hermite and dofs.size == self._dim and np.all(dofs >= 4):
            self._dofs = dofs
            self._dof_n = np.prod(self._dofs)
        else:
            print("dofs are of incorrect type or value.")
        if self._dofs is not None:
            self._dofs_vector = np.zeros(np.prod(self._dofs))
        
            
    def setQuadPoints(self, quadrature_points):
        quadrature_points = np.array(quadrature_points)
        if self._basis_function == BasisFunction.Lagrange:
            print("No need for quadrature points")
        elif self._basis_function == BasisFunction.Hermite \
            and quadrature_points.size == self._dim \
            and np.all(self._dofs <= quadrature_points):
            self._quad = quadrature_points
            self._quad_coords_vector = np.array([GLLSpace(LIMITS, x) for x in self._quad], dtype = object)
            self._weight_vector = np.array([GLLWeights(LIMITS, x) for x in self._quad], dtype = object)
            self._quad_coords_mesh = np.array(np.meshgrid(*self._quad_coords_vector, indexing='ij'), dtype = float).T.reshape(-1, self._dim)
            self._quad_n = np.prod(self._quad)
        else:
            print("oh oh, something is wrong")
    
    def _getBasisFunction(self):
        if self._basis_function == BasisFunction.Lagrange:
            return 
        elif self._basis_function == BasisFunction.Hermite:
            pass
    
    def getMassMatrix1D(self, dim):
        if dim < self._dim:
            x = self.getCoordsVector(dim)
            if self._basis_function == BasisFunction.Lagrange:
                H = np.array([LagrangePolynomialGLLBased(i, self._quad_coords_vector[dim], x.size) for i in range(x.size)])
                H = np.round(H, 12)
                H[H == .0] = .0
                return H
            elif self._basis_function == BasisFunction.Hermite:
                H12 = np.array([Hermite(x, i) for i in range(2)]).T
                H34 = np.array([Hermite(x, i) for i in range(2, 4)]).T
                H_extra = np.array([Hermite(x, i) for i in range(4, self._dofs[dim])]).T
                if H_extra.size > 0:
                    H = np.concatenate([H12, H_extra, H34], axis=1)
                else:
                    H = np.concatenate([H12, H34], axis=1)
                return H
        else:
            print("ohoihioh")
            
    def getWeightMatrix(self):
        W = 1
        for counter in range(self._dim - 1, -1, -1):
            W = np.kron(W, np.diag(self.getWeightVector(counter)))
        return W
            
    def getMassMatrix(self):
        H = 1
        for counter in range(self._dim - 1, -1, -1):
            H = np.kron(H, self.getMassMatrix1D(counter))
        return H
    
    def getCoordsVector(self, dim):
        return np.array(self._quad_coords_vector[dim], np.float)
    
    def getWeightVector(self, dim):
        return np.array(self._weight_vector[dim], np.float)
    
    def getFieldOperator(self, dirs):
        D = 1
        if self._basis_function == BasisFunction.Lagrange:
            for counter in range(self._dim - 1, -1, -1):
                if dirs[counter] == 0:
                    aux = self.getMassMatrix1D(counter)
                elif dirs[counter] == 1:
                    aux = LagrangeDerivativePolynomialGLLBased(self._dofs[counter])
                else:
                    print("Something went bananas")
                    return
                D = np.kron(D, aux)
        elif self._basis_function == BasisFunction.Hermite:
            for counter in range(self._dim - 1, -1, -1):
                x = self.getCoordsVector(counter)
                if dirs[counter] <= 2 and dirs[counter] >= 0:
                    H12 = np.array([Hermite(x, i, dirs[counter]) for i in range(2)]).T
                    H34 = np.array([Hermite(x, i, dirs[counter]) for i in range(2, 4)]).T
                    H_extra = np.array([Hermite(x, i, dirs[counter]) for i in range(4, self._dofs[counter])]).T
                    if H_extra.size > 0:
                        H = np.concatenate([H12, H_extra, H34], axis=1)
                    else:
                        H = np.concatenate([H12, H34], axis=1)
                    H = np.round(H, 12)
                    H[H == .0] = .0
                else:
                    print("Cannot choose a higher derivative than 2 for Hermite")
                D = np.kron(D, H)
        return D
    
    def getBoundaryIndex(self, direction, boundary):
        value = -1 if boundary == 0 else 1
        if direction < self._dim:
            return np.where( np.abs( self._quad_coords_mesh[:, direction] -  value ) < TOL )
        else:
            print("Problems in getBoundaryIndex")
            return
    
    def getBoundaryOperator(self, direction, boundary, derivative):
        if direction < self._dim \
            and len(derivative) == self._dim:
            B = np.zeros([np.prod(self._quad), np.prod(self._dofs)])
            B[self.getBoundaryIndex(direction, boundary), :]\
                = self.getFieldOperator(derivative)[self.getBoundaryIndex(direction, boundary), :]
            return B
        else:
            print("Whoopsie, problems with the boundary dimensions")
            return

def test_2D():
    e = Element(2)
    e.setBasisFunction(BasisFunction.Hermite)
    
    e.setDof([5, 5])
    e.setQuadPoints([12, 8])
    H = e.getFieldOperator([0, 0])
    Dx = e.getFieldOperator([1, 0])
    Dy = e.getFieldOperator([0, 1])
    
    B_valy0 = e.getBoundaryOperator(1, 0, [0, 0])
    B_valx0 = e.getBoundaryOperator(0, 0, [0, 0])
    
    
    K = Dx + Dy
    G = np.ones([np.prod(e._quad), 1]) * 3/4
    Zv = np.zeros(G.shape)
    Gbc1 = np.zeros(G.shape)
    Gbc2 = np.zeros(G.shape)
    Kbc1 = B_valy0
    Kbc2 = B_valx0
    indx1 = e.getBoundaryIndex(1, 0)
    indx2 = e.getBoundaryIndex(0, 0)
    Gbc1[indx1] = e._quad_coords_mesh[indx1, 0].reshape([-1, 1])/2 - 1/4
    Gbc2[indx2] = e._quad_coords_mesh[indx2, 1].reshape([-1, 1])/4 - 1/2
    W = e.getWeightMatrix()
    A = K.T @ W @ K + Kbc1.T @ W @ Kbc1 + Kbc2.T @ W @ Kbc2
    F = K.T @ W @ G + Kbc1.T @ W @ Gbc1 + Kbc2.T @ W @ Gbc2
    
    alpha = np.linalg.solve(A, F)
    
    X, Y = np.meshgrid(e._quad_coords_vector[0], e._quad_coords_vector[1])
    Z = H @ alpha
    Z_analytical = X/2 + Y/4
    
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    ax.plot_surface(X, Y, Z.reshape(X.shape))
    plt.show()

if __name__ == "__main__":
    test_2D()
    
