import numpy as np
import matplotlib.pyplot as plt
from scipy.special import eval_legendre
from enum import Enum

class BasisFunction(Enum):
    Lagrange = 1
    Hermite = 2


BASIS_HERMITIANS_D0 = (lambda x: .5 - .75 * x + .25* x**3, # 0
                       lambda x: .25 - .25 * x - .25 * x**2 + .25 * x**3, #1
                       lambda x: .5 + .75*x - .25 * x**3, # 2
                       lambda x: -.25 - .25*x + .25 * x**2 + .25*x**3, # 3
                       lambda x: .19764235376052372 * (1 - x**2)**2, # 4
                       lambda x: .23385358667337133 * (1 - x**2)**2 * x, # 5
                       lambda x: .044194173824159216 * (1 - x**2)**2 * (-7 * x**2 + 1), # 6
                       lambda x: .14657549249448218 * (1 - x**2)**2*(3*x**2 - 1)*x, # 7
                       lambda x: .019918044974971814 * (1 - x**2)**2 * (33 * x**4 - 18 * x**2 +  1), # 8                       
                       lambda x: .007131804134181851 * x*(x**2 - 1)**2*(143*x**4 - 110*x**2 + 15),
                       #lambda x: .08832026239770804 * (1 - x**2)**2 * (143 * x**4 - 110 * x**2 + 15) * x, # 9
                       lambda x: .011388577919619728 * (1 - x**2)**2 * (143 * x**6 - 143 * x**4 + 33 * x**2 - 1), # 10
                       lambda x: .012039871099548781 * (1 - x**2)**2 * (221 * x**6 - 273 * x**4 + 91 * x**2 - 7) * x, # 11
                       lambda x: .0010548080563814875 * (1 - x**2)**2 * (4199 * x**8 - 6188 * x**6 + 2730 * x**4 - 364 * x**2 + 7)) # 12

BASIS_HERMITIANS_D1 = (lambda x : -.75 + .75 * x**2, # 0
                       lambda x : -.25 - .5 * x + .75 * x**2,
                       lambda x : .75 - .75 * x**2, # 2
                       lambda x : -.25 + .5 * x + .75 * x**2,
                       lambda x : .39528470752104744 * (1 - x**2) * (-2 * x), # 4
                       lambda x : .23385358667337133 * (2 * (1 - x**2) * (-2 * x**2) +  (1 - x**2)**2 ),
                       lambda x : .044194173824159216 * (2*(1 - x**2) * (-2 * x)*(-7 * x**2 +1) + (-14 * x)* (1 - x**2)**2), # 6
                       lambda x : .14657549249448218 * ( 2*(1 - x**2) * (-2 * x) * (3 * x**2 - 1)*x +  (1 - x**2)**2*(9 * x**2 - 1) ),
                       lambda x : .019918044974971814 * ( 2*(1 - x**2) * (-2 * x)  *(33 * x**4 - 18 * x**2 + 1) + (1 - x**2)**2*   (4 * 33 * x**3 - 2 * 18 * x) ), # 8
                       lambda x : .02139541240254555 * (429*x**8 - 924*x**6 + 630*x**4 - 140*x**2 + 5),
                       #lambda x : .08832026239770804 * ((1 - x**2)**2 * (715 * x**4 - 330 * x**2 + 15) + 4 * x**2*(x**2 - 1) * (143 * x**4 - 110 * x**2 + 15) ),
                       lambda x : .011388577919619728 * ((1 - x**2)**2 * (858 * x**5 - 572 * x**3 + 66 * x) + 4 * x*(x**2 - 1) * (143 * x**6 - 143 * x**4 + 33 * x**2 - 1) ), # 10
                       lambda x : .012039871099548781 * ((1 - x**2)**2 * (1547 * x**6 - 1365 * x**4 + 273 * x**2 - 7) + 4 * x**2*(x**2 - 1) * (221 * x**6 - 273 * x**4 + 91 * x**2 - 7) ),
                       lambda x : .0010548080563814875 * (4 * x * (x**2 - 1) * (4199 * x**8 - 6188 * x**6 + 2730 * x**4 - 364 * x**2 + 7) - (1 - x**2)**2 * (-33592 * x**7 + 37128 * x**5 - 10920 * x**3 + 728 * x) )) # 12

BASIS_HERMITIANS_D2 = (lambda x : 1.5 * x, #0
                       lambda x : (-.5 + 1.5 * x),
                       lambda x : -1.5 * x, #2
                       lambda x :  (.5 + 1.5 * x),
                       lambda x : .19764235376052372 * (12 * x**2 - 4), #4
                       lambda x : 0.23385358667337133 * (12 * x * (x**2 - 1) + 8 * x**3 ),
                       lambda x : 0.044194173824159216 * ((-12 * x**2 + 4) * (7 * x**2 - 1)-112 * (x**2 - 1) * x**2 - 14 * (x**2 - 1)**2 ), # 6
                       lambda x : 0.14657549249448218 * (18 * x * (1 - x**2)**2 + 48 * x**3 * (x**2 - 1) + 8 * x**3 * (3 * x**2 - 1) + 12 * x * (x**2 - 1) * (3 * x**2 - 1) ),
                       lambda x : 0.019918044974971814 * ((12 * x**2 - 4) * (33 * x**4 - 18 * x**2 + 1) + (1 - x**2)**2 * (396 * x**2 - 36) - 8 * x * (x**2 - 1) * (-132 * x**3 + 36 * x) ), # 8
                       lambda x : .1711632992203644*x*(429*x**6 - 693*x**4 + 315*x**2 - 35),
                       #lambda x : 0.00713180413418185 * ((1 - x**2)**2 * (2860 * x**3 - 660 * x) + (x**2 - 1) * (6292 * x**5 - 3080 * x**3 + 180 * x) + 8 * x**3 * (143 * x**4 - 110 * x**2 + 15) ),
                       lambda x : 0.011388577919619728 * ((x**2 - 1) * (7440 * x**6 - 5148 * x**4 + 660 * x**2 - 4) + (1 - x**2)**2 * (4290 * x**4 - 1716 * x**2 + 66) + 8 * x**2 * (143 * x**6 - 143 * x**4 + 33 * x**2 - 1) ), # 10
                       lambda x : 0.012039871099548781 * ((x**2 - 1) * (13260 * x**7 - 12012 * x**5 + 2548 * x**3 - 84 * x) + (1 - x**2)**2 * (9282 * x**5 - 5460 * x**3 + 546 * x) + 8 * x**3 * (221 * x**6 - 273 * x**4 + 91 * x**2 - 7) ),
                       lambda x : 0.0010548080563814875 * (8 * x**2 * (4199 * x**8 - 6188 * x**6 + 2730 * x**4 - 364 * x**2 + 7) + (x**2 - 1) * (285532 * x**8 - 321776 * x**6 + 98280 * x**4 - 7280 * x**2 + 28) + (1 - x**2)**2 * (235144 * x**6 - 185640 * x**4 + 32760 * x**2 - 728)) ) # 12

BASIS_HERMITIANS = (BASIS_HERMITIANS_D0, BASIS_HERMITIANS_D1, BASIS_HERMITIANS_D2)

def _LegendrePolynomial_DEPRECATED(order, x):
    # -*- coding: utf-8 -*-
    """
LegendrePolynomial takes the coordinates 'x' and computes the order-th Legendre polynomial for those points.
For order 0 and 1 the values are pre-set:

L(0) = 1 for all points
L(1) = x for all points

For order 2 and superior, a recursive relation for the Legendre polynomials is used.
The recursive relation is:

L(order + 1) =  ((2*order + 1) * x * L(order) - order * L(order - 1)) / (order + 1)
    """
    """
Author: García Hahn, Julián Nicolás
Date: 22-10-2019
    """
    if isinstance(x, (int, float, complex)) and not isinstance(x, bool):
        x = np.array([x])
    if order < 0:
        raise ValueError(
            "order should be greater than 0. Order is: " + str(order))
        # TODO: Include a warning message for when the x array comes empty or is outside -1, 1 boundaries
    elif order == 0:
        return np.ones(x.shape)

    elif order == 1:
        return x

    else:
        leg_pol_previous_2 = np.ones(x.shape)
        leg_pol_previous_1 = x
        for counter in range(1, order):
            leg_pol = ((2 * counter) + 1) * np.multiply(x, leg_pol_previous_1) - \
                counter * leg_pol_previous_2
            leg_pol /= (counter + 1)
            leg_pol_previous_2 = leg_pol_previous_1
            leg_pol_previous_1 = leg_pol
        return leg_pol


def _LegendreDerivativePolynomial_DEPRECATED(order, x):
    # -*- coding: utf-8 -*-
    """
LegendreDerivativePolynomial takes the coordinates 'x' and computes the derivative of the order-th of the Legendre polynomial for those points.
For order 0 and 1 the values are pre-set:

L'(0) = 0 for all points
L'(1) = 1 for all points

For order 2 and superior, a recursive relation for the derivative of the Legendre polynomials is used.
The recursive relation is:

L'(order + 1) =  (order + 1) L(order) + x * L'(order - 1)
    """
    '''
Author: García Hahn, Julián Nicolás
Date: 23-10-2019
    '''
    if isinstance(x, (int, float, complex)) and not isinstance(x, bool):
        x = np.array([x]).astype(float)
    if order < 0:
        raise ValueError(
            "order should be greater than 0. Order is: " + str(order))
        # TODO: Include a warning message for when the x array comes empty or is outside -1, 1 boundaries
    elif order == 0:
        return np.zeros(x.shape)
    elif order == 1:
        return np.ones(x.shape)
    else:
        x = x.astype(float)
        dev_leg_pol_previous = np.ones(x.shape)
        leg_pol_previous_2 = np.ones(x.shape)
        leg_pol_previous_1 = x
        for counter in range(1, order):
            if counter != 1:
                dev_leg_pol_previous = dev_leg_pol
                leg_pol = np.subtract(np.multiply(((2 * counter) + 1), np.multiply(x, leg_pol_previous_1)),
                                      np.multiply(counter, leg_pol_previous_2))
                leg_pol = np.divide(leg_pol, counter + 1)
                leg_pol_previous_2 = leg_pol_previous_1
                leg_pol_previous_1 = leg_pol
            dev_leg_pol = np.add(np.multiply(
                counter + 1, leg_pol_previous_1), np.multiply(x, dev_leg_pol_previous))
        return dev_leg_pol


def LegendreDerivativePolynomial(order, x):
    # -*- coding: utf-8 -*-
    """
LegendreDerivativePolynomial takes the coordinates 'x' and computes the derivative of the order-th of the Legendre polynomial for those points.
For order 0 and 1 the values are pre-set:

L'(0) = 0 for all points
L'(1) = 1 for all points

For order 2 and superior, a recursive relation for the derivative of the Legendre polynomials is used.
The recursive relation is:

L'(order + 1) =  (order + 1) L(order) + x * L'(order - 1)
    """
    '''
Author: García Hahn, Julián Nicolás
Date: 23-10-2019
    '''
    if type(x) is not np.ndarray:
        x = np.array([x])
    dev_leg_pol = np.zeros(np.size(x))
    position = np.where(np.absolute(x) == 1)
    dev_leg_pol[position] = np.multiply(
        np.power(x[position], order - 1), 0.5 * order * (order + 1))
    position = np.where(np.absolute(x) != 1)
    dev_leg_pol[position] = np.divide(np.multiply(np.subtract(np.multiply(x[position], eval_legendre(
        order, x[position])), eval_legendre(order - 1, x[position])), order), np.power(x[position], 2) - 1)
    return dev_leg_pol


def _LDevWithExtremes(order, x):
    # TODO: Document this function
    dev_leg_pol = np.zeros(np.size(x))
    dev_leg_pol[np.array((0, -1))] = np.multiply(
        np.power(np.array([-1, 1]), order - 1), order * (order + 1) * 0.5)
    dev_leg_pol[1:-1] = LegendreDerivativePolynomial(order, x[1:-1])
    return dev_leg_pol


def GaussLegendreQuadrature(order):
    # TOASK: Ask Carlos where this is from.
    # TODO: Document this function
    A = np.zeros([order, order])

    A[0, 1] = 1
    if order > 2:
        for counter in range(1, order - 1):
            A[counter, counter - 1] = counter / (2 * counter + 1)
            A[counter, counter + 1] = (counter + 1)/(2 * counter + 1)

    A[-1, -2] = (order - 1)/(2 * order - 1)
    # Index 0 so it can only take the value of the weights of the eugenvalues
    points = np.sort(np.linalg.eig(A)[0])

    weights = 2 / np.multiply((1 - np.power(points, 2)),
                              np.power(LegendreDerivativePolynomial(order, points), 2))

    return (points, weights)


def Weights(order, p):
    # TODO: Document this function
    return 2/(order * (order - 1) * np.multiply(p, p))


def _GaussLobattoLegendreQuadrature_DEPRECATED(order):
    # TODO: Document this function
    if order >= 3:
        GL_points, _ = GaussLegendreQuadrature(order - 1)
        tol = 1e-14
        points = np.ones(order)
        points[0] = -1
        points[1:-1] = np.divide(np.add(GL_points[:-1], GL_points[1:]), 2)

        p_old = np.zeros(order)
        while np.any(np.abs(np.subtract(p_old, points)) > tol):
            p_old = points
            points = np.add(p_old, np.divide(np.multiply((1 - np.power(p_old, 2)), _LDevWithExtremes(order - 1, p_old)),
                                             np.multiply((order - 1) * order, eval_legendre(order - 1, p_old))))
    elif order == 2:
        points = np.array([-1, 1])
    weights = Weights(order, eval_legendre(order - 1, points))

    return (points, weights)


def GaussLobattoLegendreQuadrature(order):
    '''
this documents
    '''
# TODO: Documents this function
    if order >= 3:
        GL_points, _ = GaussLegendreQuadrature(order - 1)
        tol = 1e-14

        points = np.ones(order)
        points[0] = -1
        points[1:-1] = np.divide(np.add(GL_points[:-1], GL_points[1:]), 2)

        p_old = np.zeros(order)
        while np.any(np.abs(p_old - points) > tol):
            p_old = points
            points = np.add(p_old, np.divide(np.multiply((1 - np.power(p_old, 2)), _LDevWithExtremes(order - 1, p_old)),
                                             np.multiply((order - 1) * order, eval_legendre(order - 1, p_old))))
    elif order == 2:
        points = np.array([-1, 1])
    elif order == 1:
        points = np.array([0])
        return(points, np.array([2]))
    weights = Weights(order, eval_legendre(order - 1, points))

    return (points, weights)


def GLLSpace(x, points):
    xp, _ = GaussLobattoLegendreQuadrature(points)
    x_min = np.min(x)
    x_max = np.max(x)
    xp = (((xp + 1) / 2) * (x_max - x_min)) + x_min
    return xp

def GLLWeights(x, points):
    _, wp = GaussLobattoLegendreQuadrature(points)
    x_min = np.min(x)
    x_max = np.max(x)
    wp = ((wp) / 2) * (x_max - x_min)
    return wp


def LagrangePolynomialGLLBased(num_pol, x, order):
    xp, _ = GaussLobattoLegendreQuadrature(order)

    term1 = np.multiply(np.multiply((order - 1) * (order),
                                    x - xp[num_pol]), eval_legendre(order - 1, xp[num_pol]))
    term2 = np.multiply(np.power(x, 2) - 1,
                        LegendreDerivativePolynomial(order - 1, x))
    ret = np.zeros(np.size(x))
    limits = np.logical_and(np.absolute(
        term1 * 1e-5) < np.finfo(float).eps, np.absolute(term2 * 1e-5) < np.finfo(float).eps)
    ret[np.where(limits)] = 1
    ret[np.where(np.logical_not(limits))] = np.divide(
        term2[np.where(np.logical_not(limits))], term1[np.where(np.logical_not(limits))])

    return ret


def LagrangeDerivativePolynomialGLLBased(order):
    # -*- coding: utf-8 -*-
    """
LagrangeDerivativePolynomialGLLBased takes the order of the original Lagrange polynomials and computes the derivative matrix for the Lagrange polynomials at Gauss Legendre Lobatto colocated points.
The Original author was Funaro. I cannot find the original paper so far.

There are preset values for the returning matrix. Let the matrix be called 'd' as in the original paper. Then:

d(0, 0) = - order * (order + 1) / 4
d(order+1, order+1) = order * (order + 1) / 4

if i == j:
  d(i, j) = 0

else:
  d(i, j) = eval_legendre(order, x_i) / ( eval_legendre(order, x_j) * (x_i - x_j))

    """
    '''
Author: García Hahn, Julián Nicolás
Date: 21-06-2020
    '''

    xp, _ = GaussLobattoLegendreQuadrature(order)
    legendre_values = eval_legendre(order - 1, xp)
    term1 = np.outer(legendre_values, 1/legendre_values)
    term2 = np.repeat(np.array([xp]), order, axis=0)
    term2 = term2.T - np.array([xp])

    bool_mask = np.eye(order, dtype=bool)
    term2[bool_mask] = 1
    term1 = term1 / term2
    term1[bool_mask] = 0

    term1[0, 0] = - ((order - 1) * order) / 4
    term1[-1, -1] = -term1[0, 0]

    return term1

def Hermite(x, n, der = 0):
    return BASIS_HERMITIANS[der][n](x)
                        
"""
basis_hermitians = [lambda x: .5 - .75 * x + .25* x**3,
                        lambda x: .25 - .25*x - .25*x ** 2+.25*x ** 3),
                        lambda x: .5+.75*x-.25*x ** 3,
                        lambda x: (-.25-.25*x+.25*x ** 2+.25*x ** 3),
                        lambda x: sqrt(.0390625)*(1-x ** 2) ** 2,
                        lambda x: sqrt(7/128)*(1-x ** 2) ** 2*x,
                        lambda x: 1/6*sqrt(9/128) * \
                        (1-x ** 2) ** 2*(-7*x ** 2 + 1),
                        lambda x: .5*sqrt(11/128) * \
                        (1-x ** 2) ** 2*(3*x ** 2-1)*x,
                        lambda x: 1/16*sqrt(13/128)*(1-x **
                                                     2) ** 2*(33*x ** 4-18*x ** 2+1),
                        lambda x: .258 * \
                        sqrt(15/128)*(1-x ** 2) ** 2 * \
                        (143*x ** 4-110*x ** 2+15)*x,
                        lambda x: 1/32 * \
                        sqrt(17/128)*(1-x ** 2) ** 2 * \
                        (143*x ** 6-143*x ** 4+33*x ** 2-1),
                        lambda x: 1/32 * \
                        sqrt(19/128)*(1-x ** 2) ** 2 * \
                        (221*x ** 6-273*x ** 4+91*x ** 2-7)*x,
                        lambda x: 1/384*sqrt(21/128)*(1-x ** 2) ** 2*(4199*x ** 8-6188*x ** 6+2730*x ** 4-364*x ** 2+7)]
"""


def testing_function(order):
    xp, _ = GaussLobattoLegendreQuadrature(order)
    x = np.linspace(-1, 1, 10000)
    z = np.zeros(len(xp))
    o = np.ones(len(xp))
    color = (1, 0.6, 0,)
    #funct = lambda x: np.ones(x.size)
    def funct(x): return np.sin(x * np.pi * 4) * 0.2 + np.exp(x)
    fig, ax = plt.subplots()
    ax.scatter(xp, z, s=80, facecolors='none', edgecolors=color)
    ax.scatter(xp, o * funct(xp), s=80, facecolors='none', edgecolors=color)

    basis = np.array([LagrangePolynomialGLLBased(
        num_pol, x, order) * funct(xp[num_pol]) for num_pol in range(len(xp))])
    derivative = LagrangeDerivativePolynomialGLLBased(order)

    ax.plot(x, basis.T, lw=0.2)

    ax.plot(x, funct(x), lw=2, alpha=0.4,
            color='orange', label="Posta function")
    ax.plot(x, np.sum(basis, axis=0), lw=2,
            color='purple', label="Approximated Function")
    ax.plot(x, np.sum(derivative, axis=0), lw=2, color='red',
            label="Derivated Approximated Function")
    ax.legend(loc='best')

    ax.grid(True)
    #fig.savefig('Lagrange Interpolation.png', dpi = 600)
    plt.show()

if __name__ == "__main__":
    x = np.linspace(-1, 1, 1000000)
    fig, axs = plt.subplots()
    d = 1
    p = 7
    x2 = x[:-1]+np.diff(x)[0]/2
    
    dx = np.diff(x)[0]
    
    #axs.plot(x2, (np.diff(Hermite(x, p, d))/dx - np.interp(x2, x, Hermite(x, p, d+1))), lw = 0.5)
#    y = np.array([Hermite(x, i) for i in range(1)])
    axs.plot(x, Hermite(x, 4, 0), lw = 0.5)
    axs.plot(x, Hermite(x, 5, 0), lw = 0.5)
    axs.plot(x, Hermite(x, 6, 0), lw = 0.5)
    axs.plot(x, Hermite(x, 7, 0), lw = 0.5)
    #axs.plot(x, Hermite(x, 9, 2), lw = 0.5)
    axs.grid(True)
    plt.show()