import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
from highorder import GaussLobattoLegendreQuadrature as GLL

def CompareText(analytic, numerical):
    print("Analytical integral is:", analytic)
    print("Numerical integral is:", numerical)
    print("Difference between both is:", np.abs(analytic - numerical))

# Set iteration to check points in integration
iteration_points = 20

# Limits of integration
lims = np.array([-1, 1])

# Set of functions and integral
integrand = lambda x: np.exp(x)
integral = lambda x: np.exp(x)

# Value of integral
integral_value = np.diff(integral(lims))
# Storage of numeric results
numerical_results = np.zeros(iteration_points)

for counter in range(iteration_points):
    # Ask for points and weights
    points, weights = GLL( counter + 1 )
    # Re-scaling the points
    weights = (weights / (2)) * np.diff(lims)
    points = (points / (2)) * np.diff(lims)
    points = points + np.diff(lims) / 2 + lims[0]
    numerical_results[counter] = np.sum(integrand(points) * weights)

# Saving results
np.save('results_1', numerical_results)
    
# Drawings
fig, ax = plt.subplots()

# x = np.linspace(lims[0], lims[1], 100)
# ax.plot(x, integrand(x))
# ax.plot(x, integral(x)-integral(0), color='g')
print(np.size(numerical_results))
# ax.set_yscale('log')
# ax.set_xscale('log')
error_L1 = np.abs(numerical_results - integral_value)
ax.set_yscale('log')
ax.set_xscale('log')
ax.autoscale()
ax.set_ylim((1e-20, 1e5))
ax.scatter(np.arange(1,iteration_points + 1), error_L1)
ax.plot(np.arange(1,iteration_points + 1), error_L1, lw = 1)
ax.set_xticks(np.arange(1, iteration_points+1))
# ax.set_xticks(np.linspace(1, iteration_points+1, (iteration_points+1) * 10), minor=True)
ax.xaxis.set_minor_formatter(NullFormatter())
ax.set_xticklabels(np.arange(1, iteration_points+1))
ax.set_yticks(np.logspace(-20, 5, 6))
ax.set_yticks(np.logspace(-20, 5, 26), minor = True)
ax.yaxis.set_minor_formatter(NullFormatter())
ax.grid(b = True, which = 'major', color='k', linestyle='-')
ax.grid(b = True, which = 'minor', color='gray', linestyle='--', alpha=0.5)
# ax.set_ylim((np.min(error_L1), np.max(error_L1)))
# ax.scatter(points, integrand(points))
# ax.axvline(lims[0], color = 'k', linewidth = 2)
# ax.axvline(lims[1], color = 'k', linewidth = 2)

print(np.abs(numerical_results - integral_value))
plt.show()
